#!/usr/bin/env bash
# Temporary bash script to track launched simulations

DIR=/home/jupyter/home/1998marcom/colloquio/data/script002
if [ -z $1 ] && [ -d $DIR ]; then
	echo "Sei un idiota la cartella esiste già"
	return 42 2>/dev/null
	exit 42
else
	mkdir $DIR
fi

# d=2

## n=2 (full)
###screen -d -m ./FlowLauncher.py -m 0.3 -M 0.6 -r 19 -R 4 -i 6 -t 150000 -T 1000000 -s 5 -v 2 -V 6 -e 0 -n 2 -d 2 -o $DIR/flow001 -B 0.03 ../build/sierpinski_reductor

## n=3
###screen -d -m ./FlowLauncher.py -m 0.5 -M 0.8 -r 19 -R 4 -i 6 -t 100000 -T 1000000 -s 4 -v 2 -V 5 -e 0 -n 3 -d 2 -o $DIR/flow002 -B 0.02 ../build/sierpinski_reductor

## n=4 (scale 3 vuol dire schifo)
###screen -d -m ./FlowLauncher.py -m 0.6 -M 1.2 -r 19 -R 4 -i 6 -t 100000 -T 1000000 -s 3 -v 2 -V 4 -e 0 -n 4 -d 2 -o $DIR/flow003 -B 0.02 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.4 -M 2.4 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 1 -n 4 -d 2 -o $DIR/flow004 -B 0.05 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.4 -M 2.4 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 2 -n 4 -d 2 -o $DIR/flow005 -B 0.05 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.4 -M 2.4 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 3 -n 4 -d 2 -o $DIR/flow006 -B 0.05 ../build/sierpinski_reductor

## n=5
###screen -d -m ./FlowLauncher.py -m 0.9 -M 1.5 -r 19 -R 4 -i 6 -t 100000 -T 1000000 -s 3 -v 2 -V 4 -e 0 -n 5 -d 2 -o $DIR/flow007 -B 0.02 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.4 -M 2.4 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 1 -n 5 -d 2 -o $DIR/flow008 -B 0.05 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.4 -M 2.4 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 2 -n 5 -d 2 -o $DIR/flow009 -B 0.05 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.4 -M 2.4 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 3 -n 5 -d 2 -o $DIR/flow010 -B 0.05 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.4 -M 2.4 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 4 -n 5 -d 2 -o $DIR/flow011 -B 0.05 ../build/sierpinski_reductor

## n=6
###screen -d -m ./FlowLauncher.py -m 1.0 -M 2.4 -r 19 -R 4 -i 8 -t 100000 -T 1000000 -s 3 -v 2 -V 4 -e 0 -n 6 -d 2 -o $DIR/flow012 -B 0.02 ../build/sierpinski_reductor


# d=3

## n=2
screen -d -m ./FlowLauncher.py -m 0.18 -M 0.4 -r 19 -R 4 -i 7 -t 150000 -T 1000000 -s 4 -v 2 -V 5 -e 0 -n 2 -d 3 -o $DIR/flow013 -B 0.02 ../build/sierpinski_reductor

## n=3
screen -d -m ./FlowLauncher.py -m 0.18 -M 1.0 -r 19 -R 4 -i 8 -t 100000 -T 1000000 -s 3 -v 2 -V 4 -e 0 -n 3 -d 3 -o $DIR/flow014 -B 0.03 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.18 -M 2.2 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 1 -n 3 -d 3 -o $DIR/flow015 -B 0.05 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.18 -M 2.2 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 2 -n 3 -d 3 -o $DIR/flow016 -B 0.05 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.18 -M 2.2 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 3 -n 3 -d 3 -o $DIR/flow017 -B 0.05 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.18 -M 2.2 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 4 -n 3 -d 3 -o $DIR/flow018 -B 0.05 ../build/sierpinski_reductor
#screen -d -m ./FlowLauncher.py -m 0.18 -M 2.2 -r 23 -R 5 -i 8 -t 100000 -T 1000000 -s 2 -v 1 -V 3 -e 5 -n 3 -d 3 -o $DIR/flow019 -B 0.05 ../build/sierpinski_reductor
screen -d -m ./FlowLauncher.py -m 0.18 -M 0.5 -r 19 -R 4 -i 7 -t 100000 -T 1000000 -s 3 -v 2 -V 4 -e 6 -n 3 -d 3 -o $DIR/flow020 -B 0.02 ../build/sierpinski_reductor


# d=4

## n=2
screen -d -m ./FlowLauncher.py -m 0.1 -M 0.3 -r 19 -R 4 -i 6 -t 150000 -T 1000000 -s 3 -v 2 -V 4 -e 0 -n 2 -d 4 -o $DIR/flow021 -B 0.01 ../build/sierpinski_reductor


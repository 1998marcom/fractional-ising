import pathlib
import subprocess
import numpy as np

base_executable = ""

class Run:
    args = {}
    path = ""
    def __init__(self, path, args={}):
        if args:
            # Allora è anche da lanciare
            arg_list = [str(pathlib.Path(base_executable).resolve())]
            for name, arg in args.items():
                arg_list.extend(["--"+name, arg])
            if "-o" not in arg_list:
                arg_list.extend(["-o", path])
            arg_list = list(map(str, arg_list))
            self.path = path
            subprocess.run(arg_list, stdout=subprocess.DEVNULL)
        # Ora è da parsare
        with open(str(path)+"_header","r") as headerF:
            for line in headerF.readlines():
                words = line.split()
                self.args[words[0]] = words[1]
        self.path = path
        self.data = np.loadtxt(str(path), unpack=True) # self.data = beta, energy, magnetization
        self.data_blocked = np.loadtxt(str(path)+"_blocked", unpack=True)
        # Seghiamo la termalizzazione sotto buone ipotesi:
        energy, *_ = self.data
        condition = (energy >= np.median(energy))
        intcond = list(map(int, list(condition)))
        first_ix = intcond.index(1)
        self.data = tuple(column[first_ix:] for column in self.data)
        self.data_blocked = tuple(column[first_ix:] for column in self.data_blocked)


from .RGTempSimulation import *
from .RGFieldSimulation import *
from .FSSSimulation import *


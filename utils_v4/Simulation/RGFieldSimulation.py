import subprocess
import pathlib
from copy import deepcopy
import numpy as np
from . import Run


class RGFieldSimulation:

    args = {}
    path = ""

    ff = np.array([])
    u_blocked = np.array([])
    u_reduced = np.array([])
    m_blocked = np.array([])
    m_reduced = np.array([])
    s_blocked = np.array([])
    s_reduced = np.array([])
    
    def __init__(self, path, args={}):

        self.path = pathlib.Path(path)
        
        if args:
            # Allora è da lanciare
            self.path.mkdir()
            self.args = args
            with open(self.path/"header", "w") as header:
                for name, arg in args.items():
                    header.write("{a} {b}\n".format(a=name, b=arg))
            iter_args = deepcopy(self.args)
            _iter = int(self.args["iter"])
            _runs = int(self.args["runs"])
            _selected_runs = int(self.args["selected-runs"]) # We are going to select 2*selected_runs
            del iter_args["iter"]
            del iter_args["runs"]
            iter_args["length"] = self.args["runs"]
            del iter_args["selected-runs"]
            _min = -float(self.args["field"])
            _max = float(self.args["field"])
            for it in range(_iter):
                print("Beginning iter {} of {}.".format(it, _iter), flush=True)
                if it == 0:
                    iter_args["min"] = _min
                    iter_args["max"] = _max
                else:
                    def find_crit_index(a, b):
                        return (a < b).sum()
                        '''
                        a1 = a[::-1]
                        b1 = b[::-1]
                        if len(a1) != len(b1):
                            raise KeyError
                        for i in range(len(a1)):
                            if a1[i] > b1[i]:
                                return len(a1)-i-1
                        raise KeyError
                        '''
                    ix = find_crit_index(self.s_blocked, self.s_reduced)
                    iter_args["min"] = self.ff[max(0, ix-1-_selected_runs)]
                    iter_args["max"] = self.ff[min(len(self.ff)-1, ix+_selected_runs)]
                    iter_args["min"] = iter_args["min"]*1.03-0.03*iter_args["max"]
                    iter_args["max"] = iter_args["max"]*1.03-0.03*iter_args["min"]
                if args:
                    iteration = FieldIteration(self.path/"iter{:03d}".format(it), iter_args)
                else:
                    iteration = FieldIteration(self.path/"iter{:03d}".format(it))
                self.ff = np.concatenate([self.ff, iteration.ff])
                self.u_blocked = np.concatenate([self.u_blocked, iteration.u_blocked])
                self.u_reduced = np.concatenate([self.u_reduced, iteration.u_reduced])
                self.m_blocked = np.concatenate([self.m_blocked, iteration.m_blocked])
                self.m_reduced = np.concatenate([self.m_reduced, iteration.m_reduced])
                self.s_blocked = np.concatenate([self.s_blocked, iteration.s_blocked])
                self.s_reduced = np.concatenate([self.s_reduced, iteration.s_reduced])
                sorting_indexes = np.argsort(self.ff)
                self.ff = self.ff[sorting_indexes]
                self.u_blocked = self.u_blocked[sorting_indexes]
                self.u_reduced = self.u_reduced[sorting_indexes]
                self.m_blocked = self.m_blocked[sorting_indexes]
                self.m_reduced = self.m_reduced[sorting_indexes]
                self.s_blocked = self.s_blocked[sorting_indexes]
                self.s_reduced = self.s_reduced[sorting_indexes]
            np.savetxt(self.path/"u", np.column_stack((self.ff, self.u_blocked, self.u_reduced)))
            np.savetxt(self.path/"m", np.column_stack((self.ff, self.m_blocked, self.m_reduced)))
            np.savetxt(self.path/"s", np.column_stack((self.ff, self.s_blocked, self.s_reduced)))
        else:
            # Se è solo da parsare
            with open(self.path/"header", "r") as header:
                for line in header.readlines():
                    words = line.split()
                    self.args[words[0]] = words[1]
            self.ff, self.u_blocked, self.u_reduced = np.loadtxt(self.path/"u", unpack=True)
            self.ff, self.m_blocked, self.m_reduced = np.loadtxt(self.path/"m", unpack=True)
            self.ff, self.s_blocked, self.s_reduced = np.loadtxt(self.path/"s", unpack=True)

class FieldIteration:
    
    args = {}
    path = ""
    
    def __init__(self, path, args={}):
        
        self.path = pathlib.Path(path)
        
        if args:
            # Allora è anche da lanciare
            self.path.mkdir()
            self.args = args
            with open(path/"header", "w") as header:
                for name, arg in args.items():
                    header.write("{} {}\n".format(name, arg))
            run_args = deepcopy(self.args)
            _min = float(self.args["min"])
            _max = float(self.args["max"])
            _l = int(self.args["length"])
            del run_args["min"]
            del run_args["max"]
            del run_args["length"]
            ff = np.linspace(_min, _max, _l)
            self.ff = ff
            print("At this iteration ff is", ff)

            uF = open(self.path / "u", "w")
            uF.write("#blocked reduced\n")
            self.u_blocked = []; self.u_reduced = [] # array of averages of single runs
            mF = open(self.path / "m", "w")
            mF.write("#blocked reduced\n")
            self.m_blocked = []; self.m_reduced = []
            sF = open(self.path / "s", "w")
            sF.write("#blocked reduced\n")
            self.s_blocked = []; self.s_reduced = []

            for index, f in enumerate(ff):
                print("\tPerforming run {} of {}.".format(index, len(ff)), flush=True)
                run_args["field"] = f
                run_path_a = self.path / "{:03d}a".format(index)
                run_a = Run(run_path_a, run_args) 
                run_args_b = deepcopy(run_args)
                run_args_b["scale"] = str(int(run_args["scale"])-1)
                run_path_b = self.path / "{:03d}b".format(index)
                run_b = Run(run_path_b, run_args_b) 
                # Noi saremmo interessati a run_a_blocked confrontato con run_b
                u_blocked, m_blocked = run_a.data_blocked
                u_reduced, m_reduced = run_b.data
                self.u_blocked.append(u_blocked.mean())
                self.u_reduced.append(u_reduced.mean())
                self.m_blocked.append(abs(m_blocked).mean())
                self.m_reduced.append(abs(m_reduced).mean())
                self.s_blocked.append(m_blocked.mean())
                self.s_reduced.append(m_reduced.mean())
                uF.write("{field} {blocked} {reduced}\n".format(field=f, blocked=self.u_blocked[-1], reduced=self.u_reduced[-1]))
                mF.write("{field} {blocked} {reduced}\n".format(field=f, blocked=self.m_blocked[-1], reduced=self.m_reduced[-1]))
                sF.write("{field} {blocked} {reduced}\n".format(field=f, blocked=self.s_blocked[-1], reduced=self.s_reduced[-1]))
            uF.close()
            mF.close()
            sF.close()

        else:
            # Se è solo da parsare
            with open(path/"header", "r") as header:
                for line in header.readlines():
                    words = line.split()
                    self.args[words[0]] = words[1]
            self.ff, self.u_blocked, self.u_reduced = np.loadtxt(path/"u", unpack=True)
            self.ff, self.m_blocked, self.m_reduced = np.loadtxt(path/"m", unpack=True)
            self.ff, self.s_blocked, self.s_reduced = np.loadtxt(path/"s", unpack=True)

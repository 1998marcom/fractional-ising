import subprocess
import pathlib
from copy import deepcopy
import numpy as np
from . import Run
from uncertainties import ufloat

class FSSIteration:
    
    args = {}
    path = ""
    
    def __init__(self, path, args={}):
        
        self.path = pathlib.Path(path)
        
        if args:
            # Allora è anche da lanciare
            self.path.mkdir()
            self.args = args
            with open(path/"header", "w") as header:
                for name, arg in args.items():
                    header.write("{} {}\n".format(name, arg))
            run_args = deepcopy(self.args)
            _min = int(self.args["min-scale"])
            _max = int(self.args["max-scale"])
            del run_args["min-scale"]
            del run_args["max-scale"]
            if "field" in run_args.keys():
                print("PUPPA")
                if "/" in str(run_args["field"]):
                    run_args["field"] = run_args["field"].nominal_value

            print("FSSIteration with scale = {}-{}".format(_min, _max))

            umsF = open(self.path / "ums", "w")
            umsF.write("#scale u m s\n")
            self.scale = np.array(range(_min, _max+1))
            self.u = []; self.m = []; self.s = []

            for scale in range(_min, _max+1):
                print("\tPerforming run at scale {}.".format(scale), flush=True)
                run_args["scale"] = scale
                run_path = self.path / "{:03d}".format(scale)
                run = Run(run_path, run_args) 
                # Noi saremmo interessati a run e basta
                u, m = run.data
                self.u.append(u.mean())
                self.m.append(abs(m).mean())
                self.s.append(m.mean())
                umsF.write("{scale} {u} {m} {s}\n".format(scale=scale, u=self.u[-1], m=self.m[-1], s=self.s[-1]))
            umsF.close()
            self.u = np.array(self.u)
            self.m = np.array(self.m)
            self.s = np.array(self.s)

        else:
            # Se è solo da parsare
            with open(path/"header", "r") as header:
                for line in header.readlines():
                    words = line.split()
                    self.args[words[0]] = words[1]
            self.scale, self.u, self.m, self.s = np.loadtxt(path/"ums", unpack=True)

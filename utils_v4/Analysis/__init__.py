import pathlib

# Constants
SPAN = 0.1
DSPAN = 0.001
#POINTS = 10 # Now it's a variable with a default

analdir = pathlib.Path(__file__).parent
simdir = analdir / 'Simulation'
if not simdir.exists():
    simdir.unlink(missing_ok=True)
    simdir.symlink_to(analdir.parent/'Simulation')

from .RGTempAnalysis import *
from .RGFieldAnalysis import *
from .FSSBetaAnalysis import *
from .FSSGammaAnalysis import *

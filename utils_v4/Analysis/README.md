Analysis needs access to Simulation as a subfolder of simulation. But double copies are prevented because of .gitignore explicitly prohibiting git from considering Simulation subfolder.
On running, Analysis checks if a folder of name Simulation is available, if not, it makes the necessary softlinks.


import numpy as np
from .Simulation import RGFieldSimulation
from math import sqrt, log
from pathlib import Path
import matplotlib.pyplot as plt
from . import SPAN, DSPAN

def get_RGFieldAnalysis(path, POINTS=None):
    
    # Getting simulation at path
    print("RGFieldAnalysing", path)
    s = RGFieldSimulation(path)
    '''
    s.args = {
        "dim": args.env_dim,
        "nsplit": args.nsplit,
        "extra": args.extra,
        "scale": args.scale,
        "time": args.time,
        "min": args.min,
        "max": args.max,
        "runs": args.runs,
        "selected-runs": args.selected_runs,
        "iter": args.iter,
    }
    '''
    
    # Cose che ci servono per l'analisi:
    b     = int(s.args['nsplit'])
    scale = int(s.args['scale']) # not really needed
    envD  = int(s.args['dim'])
    extra = int(s.args['extra'])
    hausD = log(2**(envD-1)*(2+envD*(b-2))+extra) / log(b)
    spins = (2**(envD-1)*(2+envD*(b-2))+extra)**(scale-1)
    print("    Properties of Simulation:")
    print("        b     = {}".format(b))
    print("        scale = {}".format(scale))
    print("        envD  = {}".format(envD))
    print("        extra = {}".format(extra))
    print("        spins = {}".format(spins))
    print("        hausD = {}\n".format(hausD), flush=True)
    
    # I dati da analizzare
    ff = s.ff
    sb = s.s_blocked
    sr = s.s_reduced
    

    ##U part
    print("    S analysis:")

    # Gross critical field (should be 0)
    crit_index = (s.s_blocked < s.s_reduced).sum()-1
    gross_crit_field = (s.ff[crit_index] + s.ff[crit_index+1]) / 2
    print("        gross_crit_field: {}\n".format(gross_crit_field))
    
    # Extracting tangency data
    if POINTS:
        span = DSPAN
        tan_select = np.logical_and(ff>gross_crit_field-span, ff<gross_crit_field+span)
        while tan_select.sum() < POINTS: # Vogliamo scegliere abbastanza dati purché siano vicini al punto di tangenza
            span += DSPAN
            tan_select = np.logical_and(ff>gross_crit_field-span, ff<gross_crit_field+span)
            if tan_select.sum() == len(tan_select):
                break
            if span > SPAN: # E vogliamo siano vicini al punto di tangenza
                break
    else:
        tan_select = np.logical_and(abs(sb) < 0.15, abs(sr) < 0.15)
        begin_ix = list(map(int, tan_select)).index(1)
        end_ix = len(tan_select) - list(map(int, tan_select))[::-1].index(1)
        for i in range(begin_ix, end_ix):
            tan_select[i] = True


    
    fft = ff[tan_select]
    sbt = sb[tan_select]
    srt = sr[tan_select]
    
    # We want tangents
    from scipy.optimize import curve_fit
    from uncertainties import correlated_values, umath, ufloat
    def retta(xx, m, q):
        return xx*m+q
    msb, qsb = correlated_values(*curve_fit(retta, fft, sbt))
    msr, qsr = correlated_values(*curve_fit(retta, fft, srt))

    # Critical exponent theta and susceptibilities
    crit_exp_theta = log(b) / umath.log(msb/msr)
    print("        crit_exp_theta: {}".format(crit_exp_theta))
    chi_b, chi_r = msb, msr

    # Plotting
    plt.figure()
    plt.plot(ff, sb, label='Blocked', color='blue')
    plt.plot(fft, retta(fft, msb.nominal_value, qsb.nominal_value), label='Fit blocked', color='green')
    plt.plot(ff, sr, label='Reduced', color='orange')
    plt.plot(fft, retta(fft, msr.nominal_value, qsr.nominal_value), label='Fit reduced', color='red')
    plt.legend()
    plt.title('Magnetization per spin')
    plt.savefig(Path(path)/'s.png')


    ##Returning
    return crit_exp_theta, chi_b, chi_r
    
    
if __name__ == 'main':
    import sys
    get_RGFieldAnalysis(sys.argv[1])

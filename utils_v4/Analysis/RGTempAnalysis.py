import numpy as np
from .Simulation import RGTempSimulation
from math import sqrt, log
from pathlib import Path
import matplotlib.pyplot as plt
from . import SPAN, DSPAN

def get_RGTempAnalysis(path, POINTS=20):
    
    # Getting simulation at path
    print("RGTempAnalysing", path)
    s = RGTempSimulation(path)
    '''
    s.args = {
        "dim": args.env_dim,
        "nsplit": args.nsplit,
        "extra": args.extra,
        "scale": args.scale,
        "time": args.time,
        "min": args.min,
        "max": args.max,
        "runs": args.runs,
        "selected-runs": args.selected_runs,
        "iter": args.iter,
    }
    '''
    
    # Cose che ci servono per l'analisi:
    b     = int(s.args['nsplit'])
    scale = int(s.args['scale']) # not really needed
    envD  = int(s.args['dim'])
    extra = int(s.args['extra'])
    hausD = log(2**(envD-1)*(2+envD*(b-2))+extra) / log(b)
    spins = (2**(envD-1)*(2+envD*(b-2))+extra)**(scale-1)
    print("    Properties of Simulation:")
    print("        b     = {}".format(b))
    print("        scale = {}".format(scale))
    print("        envD  = {}".format(envD))
    print("        extra = {}".format(extra))
    print("        spins = {}".format(spins))
    print("        hausD = {}\n".format(hausD), flush=True)
    
    # I dati da analizzare
    bb = s.bb
    ub = s.u_blocked
    ur = s.u_reduced
    mb = s.m_blocked
    mr = s.m_reduced
    

    ##U part
    print("    U analysis:")

    # Gross critical beta
    crit_index = (s.u_blocked > s.u_reduced).sum()-1
    gross_crit_beta = (s.bb[crit_index] + s.bb[crit_index+1]) / 2
    print("        gross_crit_beta: {}\n".format(gross_crit_beta))
    
    # Extracting tangency data
    span = DSPAN
    tan_select = np.logical_and(bb>gross_crit_beta*(1-span), bb<gross_crit_beta*(1+span))
    while tan_select.sum() < POINTS: # Vogliamo scegliere abbastanza dati purché siano vicini al punto di tangenza
        span += DSPAN
        tan_select = np.logical_and(bb>gross_crit_beta*(1-span), bb<gross_crit_beta*(1+span))
        if tan_select.sum() == len(tan_select):
            break
        if span > SPAN: # E vogliamo siano vicini al punto di tangenza
            raise KeyError
    
    bbt = bb[tan_select]
    ubt = ub[tan_select]
    urt = ur[tan_select]
    
    # We want tangents
    from scipy.optimize import curve_fit
    from uncertainties import correlated_values, umath, ufloat
    def retta(xx, m, q):
        return xx*m+q
    mub, qub = correlated_values(*curve_fit(retta, bbt, ubt))
    mur, qur = correlated_values(*curve_fit(retta, bbt, urt))

    # Critical exponent nu
    crit_exp_nu = log(b) / umath.log(mub/mur)
    print("        crit_exp_nu: {}".format(crit_exp_nu))

    # Plotting
    plt.figure()
    plt.plot(bb, ub, label='Blocked', color='blue')
    plt.plot(bbt, retta(bbt, mub.nominal_value, qub.nominal_value), label='Fit blocked', color='green')
    plt.plot(bb, ur, label='Reduced', color='orange')
    plt.plot(bbt, retta(bbt, mur.nominal_value, qur.nominal_value), label='Fit reduced', color='red')
    plt.legend()
    plt.title('Energy per spin')
    plt.savefig(Path(path)/'u.png')


    ##M part
    print("    M analysis:")

    # Gross critical beta
    crit_index = (s.m_blocked < s.m_reduced).sum()-1
    gross_crit_beta = (s.bb[crit_index] + s.bb[crit_index+1]) / 2
    print("        gross_crit_beta: {}\n".format(gross_crit_beta))
    
    # Extracting tangency data
    span = DSPAN
    tan_select = np.logical_and(bb>gross_crit_beta*(1-span), bb<gross_crit_beta*(1+span))
    while tan_select.sum() < POINTS: # Vogliamo scegliere abbastanza dati purché siano vicini al punto di tangenza
        span += DSPAN
        tan_select = np.logical_and(bb>gross_crit_beta*(1-span), bb<gross_crit_beta*(1+span))
        if tan_select.sum() == len(tan_select):
            break
        if span > SPAN: # E vogliamo siano vicini al punto di tangenza
            break
    
    bbt = bb[tan_select]
    mbt = mb[tan_select]
    mrt = mr[tan_select]
    
    # We want tangents
    from scipy.optimize import curve_fit
    from uncertainties import correlated_values, umath, ufloat
    def retta(xx, m, q):
        return xx*m+q
    mmb, qmb = correlated_values(*curve_fit(retta, bbt, mbt))
    mmr, qmr = correlated_values(*curve_fit(retta, bbt, mrt))

    # Critical encounter
    crit_encounter = (qmb-qmr) / (mmr-mmb)
    print("        crit_encounter: {}".format(crit_encounter))

    # Plotting
    plt.figure()
    plt.plot(bb, mb, label='Blocked', color='blue')
    plt.plot(bbt, retta(bbt, mmb.nominal_value, qmb.nominal_value), label='Fit blocked', color='green')
    plt.plot(bb, mr, label='Reduced', color='orange')
    plt.plot(bbt, retta(bbt, mmr.nominal_value, qmr.nominal_value), label='Fit reduced', color='red')
    plt.legend()
    plt.title('Absolute magnetization per spin')
    plt.savefig(Path(path)/'m.png')

    ##Returning
    return crit_exp_nu, crit_encounter
    
    
if __name__ == 'main':
    import sys
    get_RGTempAnalysis(sys.argv[1])

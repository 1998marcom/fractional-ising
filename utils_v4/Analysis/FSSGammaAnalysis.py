import numpy as np
from .Simulation import FSSIteration
from math import sqrt, log
from pathlib import Path
import matplotlib.pyplot as plt

# Constants

def get_FSSGammaAnalysis(path):
    
    # Getting simulation at path
    print("FSSGammaAnalysing", path)
    s = FSSIteration(path)
    '''
    s.args = {
        "dim": args.env_dim,
        "nsplit": args.nsplit,
        "extra": args.extra,
        "scale": args.scale,
        "time": args.time,
        "min": args.min,
        "max": args.max,
        "runs": args.runs,
        "selected-runs": args.selected_runs,
        "iter": args.iter,
    }
    '''
    
    # Cose che ci servono per l'analisi:
    b     = int(s.args['nsplit'])
    scale = int(s.args['max-scale']) # not really needed
    envD  = int(s.args['dim'])
    extra = int(s.args['extra'])
    hausD = log(2**(envD-1)*(2+envD*(b-2))+extra) / log(b)
    spins = (2**(envD-1)*(2+envD*(b-2))+extra)**(scale-1)
    print("    Properties of Simulation:")
    print("        b     = {}".format(b))
    print("        scale = {}".format(scale))
    print("        envD  = {}".format(envD))
    print("        extra = {}".format(extra))
    print("        spins = {}".format(spins))
    print("        hausD = {}\n".format(hausD), flush=True)
    
    # I dati da analizzare
    scale = s.scale
    print(len(s.s))
    zeros = np.zeros(len(s.s))
    s = np.maximum(zeros, s.s) # Questo è un bias teorico che serve per far funzionare il codice
    
    # We want to fit with an exponential
    print("    S analysis:")
    from scipy.optimize import curve_fit
    from uncertainties import correlated_values, umath, ufloat, unumpy, core

    # Plot
    plt.figure()
    plt.scatter(scale, s, label='Numerical simulations')
    def sponenziale(xx, k, sp):
        return k*np.power(b*np.ones(len(xx)), sp*xx)
    k, gamma_over_nu = correlated_values(*curve_fit(sponenziale, scale, s))
    print("        gamma_over_nu: {}".format(gamma_over_nu))
    ss = np.logspace(log(min(scale)), log(max(scale)), 100, base=np.e)
    yy = sponenziale(ss, k, gamma_over_nu)
    #plt.errorbar(ss, unumpy.nominal_values(yy), yerr=unumpy.std_devs(yy), color='grey', label='fit')
    plt.plot(ss, unumpy.nominal_values(yy), color='grey', label='fit')
    plt.legend()
    plt.title('Magnetization per spin')
    plt.xlabel('scale')
    plt.ylabel('magnetization')
    plt.savefig(Path(path)/'s_gamma.png')
    
    # Returning
    return gamma_over_nu
    
    
if __name__ == 'main':
    import sys
    get_FSSGammaAnalysis(sys.argv[1])

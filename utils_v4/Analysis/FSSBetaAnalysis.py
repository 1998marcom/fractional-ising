import numpy as np
from .Simulation import FSSIteration
from math import sqrt, log
from pathlib import Path
import matplotlib.pyplot as plt

# Constants

def get_FSSBetaAnalysis(path):
    
    # Getting simulation at path
    print("FSSBetaAnalysing", path)
    s = FSSIteration(path)
    '''
    s.args = {
        "dim": args.env_dim,
        "nsplit": args.nsplit,
        "extra": args.extra,
        "scale": args.scale,
        "time": args.time,
        "min": args.min,
        "max": args.max,
        "runs": args.runs,
        "selected-runs": args.selected_runs,
        "iter": args.iter,
    }
    '''
    
    # Cose che ci servono per l'analisi:
    b     = int(s.args['nsplit'])
    scale = int(s.args['max-scale']) # not really needed
    envD  = int(s.args['dim'])
    extra = int(s.args['extra'])
    hausD = log(2**(envD-1)*(2+envD*(b-2))+extra) / log(b)
    spins = (2**(envD-1)*(2+envD*(b-2))+extra)**(scale-1)
    print("    Properties of Simulation:")
    print("        b     = {}".format(b))
    print("        scale = {}".format(scale))
    print("        envD  = {}".format(envD))
    print("        extra = {}".format(extra))
    print("        spins = {}".format(spins))
    print("        hausD = {}\n".format(hausD), flush=True)
    
    # I dati da analizzare
    scale = s.scale
    m = s.m
    
    # We want to fit with an exponential
    print("    M analysis:")
    from scipy.optimize import curve_fit
    from uncertainties import correlated_values, umath, ufloat, unumpy

    # Plot
    plt.figure()
    plt.scatter(scale, m, label='Numerical simulations')
    def sponenziale(xx, k, sp):
        return k*np.power(b*np.ones(len(xx)), -sp*xx)
    k, beta_over_nu = correlated_values(*curve_fit(sponenziale, scale, m))
    print("        beta_over_nu: {}".format(beta_over_nu))
    ss = np.logspace(log(min(scale)), log(max(scale)), 100, base=np.e)
    yy = sponenziale(ss, k, beta_over_nu)
    #plt.errorbar(ss, unumpy.nominal_values(yy), yerr=unumpy.std_devs(yy), color='grey', label='fit')
    plt.plot(ss, unumpy.nominal_values(yy), color='grey', label='fit')
    plt.legend()
    plt.title('Absolute magnetization per spin')
    plt.xlabel('scale')
    plt.ylabel('abs. magnetization')
    plt.savefig(Path(path)/'m_beta.png')
    
    # Returning
    return beta_over_nu
    
    
if __name__ == 'main':
    import sys
    get_FSSBetaAnalysis(sys.argv[1])

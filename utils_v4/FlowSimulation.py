import pathlib
from importlib import reload
if pathlib.Path(__file__).parent == pathlib.Path.cwd():
    import Simulation as sim
    import Analysis as anal
else:
    from . import Simulation as sim
    from . import Analysis as anal
from copy import deepcopy

class FlowSimulation:
    
    args = {}
    path = ""
    
    def __init__(self, path, exepath=None, args={}):

        # path
        self.path = pathlib.Path(path)
        sim.base_executable = exepath

        # args
        if args:
            self.path.mkdir()
            self.args = args
            with open(self.path/"header", "w") as fp:
                for name, value in args.items():
                    fp.write("{} {}\n".format(name, value))
        else:
            with open(self.path/"header", "r") as fp:
                for line in fp.readlines():
                    words = line.split()
                    self.args[words[0]] = words[1]

        # RGTemp
        if args:
            RGTemp_args = deepcopy(args)
            del RGTemp_args["field"]
            del RGTemp_args["max-scale"]
            del RGTemp_args["min-scale"]
            if "FSStime" in RGTemp_args.keys():
                del RGTemp_args["FSStime"]
        else:
            RGTemp_args = {}
        self.RGTemp = sim.RGTempSimulation(
                self.path / 'RGTemp',
                args=RGTemp_args,
                )
        nu, beta_c = anal.get_RGTempAnalysis(self.path / 'RGTemp')

        # RGField
        if args:
            RGField_args = deepcopy(args)
            del RGField_args["min"]
            del RGField_args["max"]
            del RGField_args["max-scale"]
            del RGField_args["min-scale"]
            RGField_args["beta"] = beta_c.nominal_value
            if "FSStime" in RGField_args.keys():
                del RGField_args["FSStime"]
        else:
            RGField_args = {}

        self.RGField = sim.RGFieldSimulation(
                self.path / "RGField",
                args=RGField_args,
                )
        theta, chi_b, chi_r = anal.get_RGFieldAnalysis(self.path / "RGField")

        # FSSs
        if args:
            FSS_args = deepcopy(args)
            del FSS_args["field"]
            del FSS_args["min"]
            del FSS_args["max"]
            del FSS_args["runs"]
            del FSS_args["selected-runs"]
            del FSS_args["iter"]
            del FSS_args["scale"]
            FSS_args["beta"] = beta_c.nominal_value
            if "FSStime" in FSS_args.keys():
                FSS_args["time"] = FSS_args["FSStime"]
                del FSS_args["FSStime"]
        else:
            FSS_args = {}
        self.FSSBeta = sim.FSSIteration(
                self.path / 'FSSBeta',
                args=FSS_args,
                )
        if args:
            chi0 = max(chi_b, chi_r)
            beta_over_nu = anal.get_FSSBetaAnalysis(self.path / "FSSBeta")
            chi1 = chi0 * pow(args["nsplit"], (args["max-scale"]-args["scale"]) * (1/theta - beta_over_nu))
            FSS_args["field"] = 1. / (3*chi1)
        self.FSSGamma = sim.FSSIteration(
                self.path / 'FSSGamma',
                args=FSS_args,
                )
        print("\nFlowSimulation CREATED\n")

    def load_analysis(self):
        pass #TODO

    def analyse(self, POINTS=10):
        nu, beta_c = anal.get_RGTempAnalysis(self.path / 'RGTemp', POINTS)
        theta, chi_b, chi_r = anal.get_RGFieldAnalysis(self.path / "RGField", POINTS)
        beta_nu = anal.get_FSSBetaAnalysis(self.path / "FSSBeta")
        gamma_nu = anal.get_FSSGammaAnalysis(self.path / "FSSGamma")
        return beta_c, nu, theta, beta_nu, gamma_nu


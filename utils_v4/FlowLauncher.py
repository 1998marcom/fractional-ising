#!/usr/bin/env python3

import pathlib
import argparse
from FlowSimulation import FlowSimulation
import Analysis as anal

parser = argparse.ArgumentParser(description='Launches a FlowSimulation')
parser.add_argument('-m', '--min', required=True, type=float, help='Initial minimum guess for beta critical')
parser.add_argument('-M', '--max', required=True, type=float, help='Initial maximum guess for beta critical')
parser.add_argument('-r', '--runs', default=9, type=int, help='Number of runs in a iteration (default 9)')
parser.add_argument('-R', '--selected_runs', default=2, type=int, help='Will get a range between 2*selected_runs to next iter (default 2)')
parser.add_argument('-i', '--iter', default=3, type=int, help='Number of iterations (default 3)')
parser.add_argument('-t', '--time', required=True, type=int, help='Number of steps to perform in each simulation')
parser.add_argument('-T', '--FSStime', default=None, type=int, help='Number of steps to perform in each simulation')
parser.add_argument('-s', '--scale', required=True, type=int, help='Scale of fractal')
parser.add_argument('-v', '--min-scale', required=True, type=int, help='Scale of fractal minimum for FSS')
parser.add_argument('-V', '--max-scale', required=True, type=int, help='Scale of fractal maximum for FSS')
parser.add_argument('-e', '--extra', required=True, type=int, help='Extra occupation of fractal')
parser.add_argument('-n', '--nsplit', required=True, type=int, help='Base splitting of the fractal')
parser.add_argument('-d', '--env_dim', required=True, type=int, help='Environment dim')
parser.add_argument('-o', '--outdir', required=True, type=str, help="Relative path of output dir")
parser.add_argument('-B', '--field', required=True, type=float, help="Max field to test")
parser.add_argument('simulation_executable', type=str, help='Relative path of simulation executable')
args = parser.parse_args()

flow_args = { 
            "dim": args.env_dim,
            "nsplit": args.nsplit,
            "extra": args.extra,
            "scale": args.scale,
            "min-scale": args.min_scale,
            "max-scale": args.max_scale,
            "time": args.time,
            "min": args.min,
            "max": args.max,
            "runs": args.runs,
            "selected-runs": args.selected_runs,
            "iter": args.iter,
            "field": args.field,
            }
if args.FSStime:
    flow_args["FSStime"] = args.FSStime

s = FlowSimulation(
        pathlib.Path(args.outdir),
        pathlib.Path(args.simulation_executable),
        flow_args,
)

print("Simulazione eseguita correttamente!")

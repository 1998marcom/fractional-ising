# Ising model in fractional dimensions

Monte Carlo simulations of the Ising model.

### Software stack used

Simulations are being written to use HIP, which can be implemented through AMD's ROCm, Nvidia's CUDA or simply [HIP-CPU](https://github.com/ROCm-Developer-Tools/HIP-CPU) to make it run on CPUs (this last implementation should be portable to most OSs).

For random number generation, the default Mersenne twister is being used (~2.5 times faster than stdlib's `rand()`). The default mt19937 uses 19937 bytes to store its state, meaning that even on the GPU with some hundred of blocks (MTGP32 needs one instance per block) the memory consumption is below ~10MB. On the CPU, the 20(x2)KB should be less than the L1 data cache. While running on CPU, the `<random>` standard library implementation is used, while on GPU hipRAND should provide its own ready-to-use implementation (through rocRAND or cuRAND).

For formatting output, `libfmt` is used (on Debian derivatives there is the `libfmt-dev` package, otherwise you can also get it from its [github repo](https://github.com/fmtlib/fmt)).

For parsing args `libtclap` is used.

### How to compile

``` g++ ./src/sierpinski_reductor.cpp -o ./build/sierpinski_reductor -std=gnu++17 -ltbb -lfmt -fopenmp -fpermissive -w -O3```

Yes, it's bad habit `-fpermissive` and `-w`, but I just omitted too many casts.


### How to launch

For a single run:
``` ./build/sierpinski_reductor -h```

For multiple simulations of the same system:
``` ./nested_utils/launcher.py -h```

import numpy as np
import simulation as sim
from math import sqrt, log
from pathlib import Path
import matplotlib.pyplot as plt

# Constants
SPAN = 0.2
dspan = 0.002

def get_analysis(path):
    
    # Getting simulation at path
    print("Analysing", path)
    s = sim.Simulation(path)
    '''
    s.args = {
        "dim": args.env_dim,
        "nsplit": args.nsplit,
        "extra": args.extra,
        "scale": args.scale,
        "time": args.time,
        "min": args.min,
        "max": args.max,
        "runs": args.runs,
        "selected-runs": args.selected_runs,
        "iter": args.iter,
    }
    '''
    
    # Cose che ci servono per l'analisi:
    b     = int(s.args['nsplit'])
    scale = int(s.args['scale']) # not really needed
    envD  = int(s.args['dim'])
    extra = int(s.args['extra'])
    hausD = log(2**(envD-1)*(2+envD*(b-2))+extra) / log(b)
    spins = (2**(envD-1)*(2+envD*(b-2))+extra)**(scale-1)
    print("    Properties of Simulation:")
    print("        b     = {}".format(b))
    print("        scale = {}".format(scale))
    print("        envD  = {}".format(envD))
    print("        extra = {}".format(extra))
    print("        spins = {}".format(spins))
    print("        hausD = {}\n".format(hausD), flush=True)
    
    # I dati da analizzare
    bb = s.bb
    ub = s.u_blocked
    ur = s.u_reduced
    mb = s.m_blocked
    mr = s.m_reduced
    
    # Gross critical beta
    crit_index = (s.u_blocked > s.u_reduced).sum()-1
    gross_crit_beta = (s.bb[crit_index] + s.bb[crit_index+1]) / 2
    print("    gross_crit_beta: {}\n".format(gross_crit_beta))
    
    # Extracting tangency data
    span = 0.01
    tan_select = np.logical_and(bb>gross_crit_beta*(1-span), bb<gross_crit_beta*(1+span))
    while tan_select.sum() < 10: # Vogliamo scegliere abbastanza dati purché siano vicini al punto di tangenza
        span += dspan
        tan_select = np.logical_and(bb>gross_crit_beta*(1-span), bb<gross_crit_beta*(1+span))
        if span > SPAN: # E vogliamo siano vicini al punto di tangenza
            raise KeyError
    
    bbt = bb[tan_select]
    ubt = ub[tan_select]
    urt = ur[tan_select]
    mbt = mb[tan_select]
    mrt = mr[tan_select]
    
    # We want tangents
    from scipy.optimize import curve_fit
    from uncertainties import correlated_values, umath
    def retta(xx, m, q):
        return xx*m+q
    mub, qub = correlated_values(*curve_fit(retta, bbt, ubt))
    mur, qur = correlated_values(*curve_fit(retta, bbt, urt))
    mmb, qmb = correlated_values(*curve_fit(retta, bbt, mbt))
    mmr, qmr = correlated_values(*curve_fit(retta, bbt, mrt))
    
    # Evaluating encounter_crit_beta and crit_exp-s
    encounter_crit_beta_u = (qub-qur)/(mur-mub)
    crit_exp_nu = log(b) / umath.log(mub/mur)
    print("    U analysis:")
    print("        encounter_crit_beta_u at: {}".format(encounter_crit_beta_u))
    print("        crit_exp_nu: {}".format(crit_exp_nu))
    plt.figure()
    plt.plot(bb, ub, label='Blocked', color='blue')
    plt.plot(bbt, retta(bbt, mub.nominal_value, qub.nominal_value), label='Fit blocked', color='black')
    plt.plot(bb, ur, label='Reduced', color='orange')
    plt.plot(bbt, retta(bbt, mur.nominal_value, qur.nominal_value), label='Fit reduced', color='red')
    plt.legend()
    plt.title('Energy per spin')
    plt.savefig(Path(path)/'u.png')
    
    encounter_crit_beta_m = (qmb-qmr)/(mmr-mmb)
    crit_exp_beta = crit_exp_nu * umath.log(mmb/mmr) / log(b)
    print("    M analysis:")
    print("        encounter_crit_beta_m at: {}".format(encounter_crit_beta_m))
    print("        crit_exp_beta: {}".format(crit_exp_beta))
    plt.figure()
    plt.plot(bb, mb, label='Blocked', color='blue')
    plt.plot(bbt, retta(bbt, mmb.nominal_value, qmb.nominal_value), label='Fit blocked', color='black')
    plt.plot(bb, mr, label='Reduced', color='orange')
    plt.plot(bbt, retta(bbt, mmr.nominal_value, qmr.nominal_value), label='Fit reduced', color='red')
    plt.legend()
    plt.title('Absolute magnetization per spin')
    plt.savefig(Path(path)/'m.png')
    
    # Extracting parabola data
    print("    C analysis:")
    span = 0.04
    while True:
        tan_select = np.logical_and(bb>gross_crit_beta*(1-span), bb<gross_crit_beta*(1+span))
        while tan_select.sum() < 24: # Vogliamo scegliere abbastanza dati purché siano vicini al punto di tangenza
            span += dspan
            tan_select = np.logical_and(bb>gross_crit_beta*(1-span), bb<gross_crit_beta*(1+span))
            if span > SPAN and tan_select.sum() < 8: # E vogliamo siano vicini al punto di tangenza
                raise KeyError
            elif span > SPAN and tan_select.sum() >= 8:
                break

        bbt = bb[tan_select]
        ubt = ub[tan_select]
        urt = ur[tan_select]
        mbt = mb[tan_select]
        mrt = mr[tan_select]

        # Specific heat exponent
        def rabola(xx, a, b, c):
            return a/2*(xx*xx) + b*xx + c
        acb, bcb, ccb = correlated_values(*curve_fit(rabola, bbt, ubt))
        acr, bcr, ccr = correlated_values(*curve_fit(rabola, bbt, urt))
        try:
            crit_exp_alpha = -crit_exp_nu * umath.log(acb/acr) / log(b)
        except ValueError as e:
            if span < SPAN:
                span += dspan
            else:
                raise e
        else:
            break
    print("        Analysis concluded at span: {}".format(span))
    print("        Specific heat blocked fit:", acb, bcb, ccb)
    print("        Specific heat reduced fit:", acr, bcr, ccr)
    print("        crit_exp_alpha: {}".format(crit_exp_alpha))
    plt.figure()
    plt.plot(bbt, ubt, label='Blocked', color='blue')
    plt.plot(bbt, rabola(bbt, acb.nominal_value, bcb.nominal_value, ccb.nominal_value), label='Fit blocked', color='black')
    plt.plot(bbt, urt, label='Reduced', color='orange')
    plt.plot(bbt, rabola(bbt, acr.nominal_value, bcr.nominal_value, ccr.nominal_value), label='Fit reduced', color='red')
    plt.legend()
    plt.title('Energy per spin (specific heat fits)')
    plt.savefig(Path(path)/'c.png')
    
    effD = (2 - crit_exp_alpha) / crit_exp_nu
    print("    Resulting dimension from alpha = 2 - nu * effD:")
    print("        effD = {}".format(effD))
    
    
if __name__ == 'main':
    import sys
    get_analysis(sys.argv[1])
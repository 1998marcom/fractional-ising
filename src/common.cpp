#ifndef FRAC_ISING_COMMON
#define FRAC_ISING_COMMON

#include <hip/hip_runtime.h>
#include <cstdint>

//#define GPU_COMPUTING

#ifdef GPU_COMPUTING

// We are on GPU
#define THREADS_PER_BLOCK 64
#define N_BLOCKS 128
	// note that N_BLOCKS < 200 in order for the MTGP32 to work on Nvidia platform

#else

// We are on CPU
#define THREADS_PER_BLOCK 256

#include <thread>
const auto N_BLOCKS = std::thread::hardware_concurrency();


#endif


#define FOR(i, N) for(unsigned int i=0; i<N; i++)
#define CEIL_DIV(a, b) (a-1)/b+1
#define TWO_DIMS 9

typedef struct{
	unsigned int size;
	int8_t* spins;
	int8_t* new_spins;
	uint8_t* links; // number of links
	int8_t* dummy; // così è tutto allineato
	unsigned int* linked_spin_id[TWO_DIMS];
} ModelStruct;

#endif

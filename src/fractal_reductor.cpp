#include "ising.cpp"
#include <random>
#include <cmath>
#include <iostream>
#include <map>
#include <ctime>
#include <vector>
#include <fstream>
#include <string>
#include <tclap/CmdLine.h>
#include <fmt/format.h>

typedef unsigned int uint;
using std::cout, std::endl;

std::vector<uint> next_coord(const std::vector<uint>& previous, const uint n) {
	std::vector<uint> result = previous;
	int i;
	for(i=previous.size()-1; i>=0; i--) {
		result[i] = (previous[i] + 1) % n;
		if (result[i] > 0) {
			break;
		}
	}
	if (i<0)
		return std::vector<uint>();
	else
		return result;
}

std::vector<uint> scale_coord(std::vector<uint> in, const uint n, const uint scale) {
	for (uint i=0; i <in.size(); i++) {
		in[i] *= (unsigned long long) (pow(n, scale)+0.1);
	}
	return in;
}

std::vector<uint> sum_coord(const std::vector<uint>& in1, const std::vector<uint>& in2) {
	std::vector<uint> result;
	for (uint i=0; i<in1.size(); i++) {
		result.push_back(in1[i] + in2[i]);
	}
	return result;
}

std::vector<uint> index_to_coord(unsigned long long index, const uint side, const uint dim) {
	//cout << "    Converting index " << index << " to coord:";
	std::vector<uint> coord(dim);
	for(int i=dim-1; i>=0; i--) {
		coord[i] = index % side;
		//cout<<" "<< coord[i];
		index /= side;
	}
	//cout <<endl;
	return coord;
}

unsigned long long coord_to_index(const std::vector<uint>& coord, const uint side) {
	//cout <<"    Converting coord:";
	unsigned long long index = 0;
	unsigned long long multiplier = 1;
	for(int i=coord.size()-1; i>=0; i--) {
		//cout <<" "<<coord[i];
		index += multiplier * coord[i];
		multiplier *= side;
	}
	//cout << "to index "<<index <<endl;
	return index;
}

void to_zero(std::vector<bool>& my_spins, const uint env_dim, const uint n, const uint extra_occupation, const uint scale, const uint index, const unsigned long long side) {
	//cout << " Calling zero with params: " << endl;
	//cout << "  scale "<<scale <<"  index " <<index <<endl;
	if (scale == 0) {
		my_spins[index] = false;
	} else {
		std::vector<uint> reduced_coord;
		for(uint i=0; i<env_dim; i++) {
			reduced_coord.push_back(0);
		}
		for (uint i=0; i< pow(n, env_dim) - 0.1; i++) {
			unsigned long long my_index = coord_to_index(sum_coord(scale_coord(reduced_coord, n, scale-1), index_to_coord(index, side, env_dim)), side);
			to_zero(my_spins, env_dim, n, extra_occupation, scale-1, my_index, side);
			reduced_coord = next_coord(reduced_coord, n);
		}
	}
	return;
}

void thin(std::vector<bool>& my_spins, const uint env_dim, const uint n, const uint extra_occupation, const uint scale, const unsigned long long index, const unsigned long long side) {
	//cout << " Calling thin with params: " << endl;
	//cout << "  scale "<<scale <<"  index " <<index <<endl;
	if (scale == 0) return;
	uint extra_effective_occupation = 0;

	std::vector<uint> reduced_coord;
	for(uint i=0; i<env_dim; i++) {
		reduced_coord.push_back(0);
	}
	for (uint i=0; i< pow(n, env_dim) - 0.1; i++) {
		uint my_sum = 0;
		for (uint j=0; j<reduced_coord.size(); j++) {
			my_sum += (bool) reduced_coord[j];
		}
		unsigned long long my_index = coord_to_index(sum_coord(scale_coord(reduced_coord, n, scale-1), index_to_coord(index, side, env_dim)), side);
		if ( (extra_occupation-extra_effective_occupation) || (my_sum <= 1) ) {
			if (my_sum > 1) extra_effective_occupation++;
			thin(my_spins, env_dim, n, extra_occupation, scale-1, my_index, side);
		} else {
			to_zero(my_spins, env_dim, n, extra_occupation, scale-1, my_index, side);
		}
		reduced_coord = next_coord(reduced_coord, n);
	}
	return;
}

void ret_to_model(IsingModel& model, std::vector<bool>& initial_ret, unsigned int env_dim, uint side, unsigned long long environment_spins) {
	std::map<unsigned long long, unsigned int>& id_to_i = model.id_to_i;
	unsigned int next_i = 0;
	for(unsigned long long id=0; id<environment_spins; id++) {
		if (not initial_ret[id]) continue;
		id_to_i[id] = next_i; next_i++;
		std::vector<uint> my_coord = index_to_coord(id, side, env_dim);
		unsigned long long link_index;
		for (uint j=0; j<my_coord.size(); j++) {
			std::vector<uint> link_coord = my_coord;
			link_coord[j] = my_coord[j] + 1;
			link_index = coord_to_index(link_coord, side);
			if (link_coord[j] < side and initial_ret[link_index] and id_to_i.count(link_index)) {
				model.safe_link(id_to_i[link_index], id_to_i[id]);
			}
			link_coord[j] = my_coord[j] - 1;
			link_index = coord_to_index(link_coord, side);
			if (link_coord[j] < side and initial_ret[link_index] and id_to_i.count(link_index)) {
				model.safe_link(id_to_i[link_index], id_to_i[id]);
			}
		}
	}

}

void construct(IsingModel& model, const uint env_dim, const uint n, const uint extra_occupation, const uint model_scale) {
	unsigned int size = model.cpu_model_str.size;
	std::mt19937 my_generator(time(NULL));
	// Questo costruisce un reticolo usando una riduzione frattale opportuna:
	
	unsigned long long environment_spins = (unsigned long long) (pow(n, model_scale*env_dim)+0.1);
	unsigned int side = (unsigned int) (pow(n, model_scale)+0.1);
	std::vector<bool> initial_ret(environment_spins, true);
	thin(initial_ret, env_dim, n, extra_occupation, model_scale, 0, side);
	cout << "Construction completed, beginning of copy." << endl;
	cout << "Spin vector has:" << endl;
	cout << "\tenvironment_spins: " << environment_spins << endl;
	cout << "\tside: " << side << endl;
	cout << "\tsize: " << model.cpu_model_str.size << endl;
	
	//cout << "Outputting true/false for initial_ret:" <<endl;
	//for (long long i=0; i<environment_spins; i++) {
	//	cout << "\t\t" << i << " " << (int) initial_ret[i] << endl;
	//}
	//cout <<endl;
	
	ret_to_model(model, initial_ret, env_dim, side, environment_spins);;
}

template <typename T> int sgn(T val) {
	    return (T(0) < val) - (val < T(0));
}

void majority_block(IsingModel& model, IsingModel& blocked, unsigned int n, unsigned int model_scale, unsigned int env_dim) {
	unsigned int side = (unsigned int) (pow(n, model_scale)+0.1);
	unsigned int side_blocked = (unsigned int) (pow(n, model_scale-1)+0.1);
	FOR(i, blocked.cpu_model_str.size){
		blocked.cpu_model_str.spins[i] = 0;
	}
	for(std::pair<unsigned long long, unsigned int> p: model.id_to_i) {
		std::vector<uint> coord = index_to_coord(p.first, side, env_dim);
		std::vector<uint> blocked_coord = coord;
		for(auto& elem: blocked_coord)
			elem /= n;
		blocked.cpu_model_str.spins[blocked.id_to_i[coord_to_index(blocked_coord, side_blocked)]] += model.cpu_model_str.spins[p.second];
	}
	FOR(i, blocked.cpu_model_str.size){
		blocked.cpu_model_str.spins[i] = sgn(blocked.cpu_model_str.spins[i]);
	}
	blocked.refresh_spins();
}


int main(int argc, char* argv[]) {
	
	// argv = [ env_dim, n, extra_occupation, model_scale, sim_time, beta, outfile_name ]
	// model_size = (2n-1+extra_occupation)^model_scale
	try {
		TCLAP::CmdLine parser("Programma per simulare ising su strutture simil-frattali");
		TCLAP::ValueArg<uint> ARG_env_dim(
				/* flag */ "d",
				/* name */ "dim",
				/* desc */ "Dimensions of starting lattice (max 4)",
				/* req  */ true,
				/* defau*/ 2,
				/* typed*/ "An unsigned integer");
		parser.add(ARG_env_dim);
		TCLAP::ValueArg<uint> ARG_n(
				/* flag */ "n",
				/* name */ "nsplit",
				/* desc */ "Side length of the fundamental fractal block.",
				/* req  */ true,
				/* defau*/ 2,
				/* typed*/ "An unsigned integer");
		parser.add(ARG_n);
		TCLAP::ValueArg<uint> ARG_extra(
				/* flag */ "e",
				/* name */ "extra",
				/* desc */ "Blocks are forcibly connected with (n-1)*d+1 occupied locations: here you can ask any extra number locations occupied of a block (up to a total of n^d).",
				/* req  */ true,
				/* defau*/ 0,
				/* typed*/ "An unsigned integer");
		parser.add(ARG_extra);
		TCLAP::ValueArg<uint> ARG_scale(
				/* flag */ "s",
				/* name */ "scale",
				/* desc */ "Scale of pseudo-fractal: blocks of blocks of blocks...",
				/* req  */ true,
				/* defau*/ 1,
				/* typed*/ "An unsigned integer");
		parser.add(ARG_scale);
		TCLAP::ValueArg<std::string> ARG_sim_time(
				/* flag */ "t",
				/* name */ "time",
				/* desc */ "Steps of simulation to perform: each step involves a dense Metropolis-Hastings (with settable flipping probability). \
Each 10 steps the simulation variables are recorded. Each 100 steps a Wolff step is performed and variables are printed on stdout.",
				/* req  */ true,
				/* defau*/ "10",
				/* typed*/ "An unsigned integer");
		parser.add(ARG_sim_time);
		TCLAP::ValueArg<double> ARG_pflip(
				/* flag */ "p",
				/* name */ "pflip",
				/* desc */ "Base probability of flipping for the dense metropolis-hastings",
				/* req  */ false,
				/* defau*/ 0.07,
				/* typed*/ "A floating point value");
		parser.add(ARG_pflip);
		TCLAP::ValueArg<double> ARG_beta(
				/* flag */ "b",
				/* name */ "beta",
				/* desc */ "Ising model beta parameter",
				/* req  */ true,
				/* defau*/ 0.1,
				/* typed*/ "A floating point value");
		parser.add(ARG_beta);
		TCLAP::ValueArg<double> ARG_J(
				/* flag */ "J",
				/* name */ "coupling",
				/* desc */ "Coupling parameter between the spins of the Ising model",
				/* req  */ false,
				/* defau*/ 1,
				/* typed*/ "A floating point value");
		parser.add(ARG_J);
		TCLAP::ValueArg<double> ARG_B(
				/* flag */ "B",
				/* name */ "field",
				/* desc */ "External field of the Ising model",
				/* req  */ false,
				/* defau*/ 0,
				/* typed*/ "A floating point value");
		parser.add(ARG_B);
		TCLAP::ValueArg<std::string> ARG_outfile(
				/* flag */ "o",
				/* name */ "outfile",
				/* desc */ "Where to output the simulation results",
				/* req  */ false,
				/* defau*/ "NULL",
				/* typed*/ "A relative path");
		parser.add(ARG_outfile);

		parser.parse(argc, argv);

		uint env_dim = ARG_env_dim.getValue();
		uint n = ARG_n.getValue();
		uint extra_occupation = ARG_extra.getValue();
		uint model_scale = ARG_scale.getValue();
		unsigned long long int sim_time = stoll(ARG_sim_time.getValue());
		double beta = ARG_beta.getValue();
		double J = ARG_J.getValue();
		double B = ARG_B.getValue();
		double pflip = ARG_pflip.getValue();
		
		// Building the model
		unsigned int model_size = (unsigned int) (pow(env_dim*n-env_dim+1+extra_occupation, model_scale)+0.1);
		IsingModel model(model_size);
		cout << "Beginning construction" << endl;
		
		construct(model, env_dim, n, extra_occupation, model_scale);
		cout << "Ended construction" << endl << endl;
		model.copy_graph_to_gpu();

		unsigned int model_size_blocked = (unsigned int) (pow(env_dim*n-env_dim+1+extra_occupation, model_scale-1)+0.1);
		IsingModel model_blocked (model_size_blocked);
		cout << "Beginning construction of blocked model" << endl;

		construct(model_blocked, env_dim, n, extra_occupation, model_scale-1);
		cout << "Ended construction of blocked model" << endl << endl;
		model_blocked.copy_graph_to_gpu();
		
		// Initializing a non-thermalized state
		FOR(i, model_size) {
			model.cpu_model_str.spins[i] = 1;
		}
		model.refresh_spins();

		model.beta = beta;
		model.J = J;
		model.B = B;
		
		std::string outfile_name = ARG_outfile.getValue();
		std::fstream outfile_header;
		outfile_header.open(outfile_name+"_header", std::fstream::out | std::fstream::trunc);
		outfile_header << "#env_dim\tn\textra_occ\tscale\tsim_time\tbeta\tJ\tB\tpflip\tmodel_size" << endl;
		outfile_header << env_dim << " ";
		outfile_header << n << " ";
		outfile_header << extra_occupation << " ";
		outfile_header << model_scale << " ";
		outfile_header << sim_time << " ";
		outfile_header << beta << " ";
		outfile_header << J << " ";
		outfile_header << B << " ";
		outfile_header << pflip << " ";
		outfile_header << model_size << " ";
		outfile_header.close();
		std::fstream outfile;
		std::fstream outfile_blocked;
		if (outfile_name != "NULL") {
			outfile.open(outfile_name, std::fstream::in | std::fstream::out | std::fstream::trunc);
			outfile_blocked.open(outfile_name+"_blocked", std::fstream::in | std::fstream::out | std::fstream::trunc);
			outfile << "#Energy_per_spin\tMagnetization_per_spin" << "\n";
			outfile_blocked << "#Energy_per_spin\tMagnetization_per_spin" << "\n";
		}

		// Simulating
		FOR(t, sim_time) {
			if (t%10 == 0) {
				majority_block(model, model_blocked, n, model_scale, env_dim);
				if (outfile_name != "NULL") {
					outfile << model << "\n";
					outfile_blocked << model_blocked << "\n";
				}
			}
			if(t%100) {
				model.step_metro_dense(pflip);
			} else {
				cout << "@t=" << t << "\n";
				auto qtys = model.get_quantities();
				cout << fmt::format("\tModel\tenergy_per_spin: {:03.3f}\tmagnetization: {:03.3f}\n", qtys[0], qtys[1]);
				auto qtys_blocked = model_blocked.get_quantities();
				cout << fmt::format("\tBlock\tenergy_per_spin: {:03.3f}\tmagnetization: {:03.3f}\n", qtys_blocked[0], qtys_blocked[1]);
				model.step_wolff_single_core();
			}
		}
		outfile.close();
		outfile_blocked.close();
		
		model.quit();
		model_blocked.quit();
	} catch (TCLAP::ArgException &e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
	}
	return 0;
}

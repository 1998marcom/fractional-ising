#include "ising.cpp"
#include <random>
#include <cmath>
#include <iostream>
#include <map>
#include <ctime>

typedef unsigned int uint;

double binomial(double n, double k) {
	return tgamma(n+1)/tgamma(k+1)/tgamma(n-k+1);
}

std::vector<double> binomial_frontier(double old_r, double alpha) {
	std::vector<double> result;
	std::cout << "Building binomial_frontier from r="<<old_r<<" to r="<<old_r+2<<": ";
	for(unsigned int m=0; m< alpha; m++) {
		double true_result = pow(old_r, alpha-m) * pow(2, m) * binomial(alpha, m);
		result.push_back(true_result);
		std::cout << true_result << " ";
	}
	std::cout << std::endl;
	return result;
}

unsigned int valence(std::map<unsigned int, std::set<unsigned int>> mappa, unsigned int elem) {
	for(auto it = mappa.begin(); it != mappa.end(); it++) {
		if ((*it).second.count(elem)) return (*it).first;
	}
	return 0;
}

void construct(IsingModel& model, const double& d) {
	unsigned int size = model.cpu_model_str.size;
	std::mt19937 my_generator(time(NULL));
	// Questo costruisce un reticolo usando l'espansione del binomio (r+2)^d

	std::map<unsigned int, std::set<unsigned int>> frontier; // Map key is number of available links
	frontier[int(ceil(2*d))].insert(0); // first element
	unsigned int index = 0;
	index++;
	double r = 1.;
	while (true) {
		std::map<unsigned int, std::set<unsigned int>> old_frontier = frontier;
		std::map<unsigned int, std::set<unsigned int>> new_frontier;
		std::vector<double> bin_frontier = binomial_frontier(r, d);
		r += 2.;
		// Now comes the crazy task:
		// bin_frontier[0] is the ideal inner older part (but actually there are `index` spins right now);
		// bin_frontier[1] is the number of new spins one link away from the old frontier.
		// bin_frontier[i] is the number of new spins i link away from the old frontier, hence linked only to bin_frontier[i-1]
		// Ideally, one should build the shape so that "shapes of bin_frontier[i]" go in the same direction for all values of r,
		//  but I'm _modestly_ confident that eventual small differences due to partially ignoring this might be negligible in the thermo limit.
		// Still, I would like not to have super-long direct interactions.
		// Because of this:
		//  * let's start from bin_frontier[-1] (points), on average we have d bin_frontier[-2] starting from each point,
		//     for a total of l = bin_frontier[-1]*d/2 lines, which implies that bin_frontier[-2]/l is the average length of each line.
		//  * then we have l lines, and s = l*(d-1)/4 squares,
		//  * then we have s squares, and c = s*(d-2)/6 cubes.
		//  * turkeynizing, if we have c_i i-cubes, we have also c_{i+1} = c_i*(d-i)/(2*(i+1)) (i+1)-cubes.
		//  Proviamo senza allineare una mazza.

		// PART 1: Getting the integers
		std::vector<double> cumulative_frontier;
		cumulative_frontier.push_back(bin_frontier[0]);
		std::cout << "Showing cumulative_frontier<double>: " << bin_frontier[0] <<" ";
		for (unsigned int i=1; i < bin_frontier.size(); i++) {
			cumulative_frontier.push_back(cumulative_frontier[i-1] + bin_frontier[i]);
			std::cout << cumulative_frontier[i] << " "; 
		}
		cumulative_frontier.push_back(pow(r, d));
		std::cout <<pow(r,d)<<std::endl;
		std::vector<unsigned int> cum_frontier_int;
		cum_frontier_int.push_back(index);
		std::cout << "Showing cum_frontier_int: "<<index<<" ";
		for (unsigned int i=1; i<cumulative_frontier.size(); i++) {
			double target = cumulative_frontier[i] - cum_frontier_int[i-1];
			auto my_distribution = std::uniform_real_distribution<double>();
			unsigned int low_target = (unsigned int) target;
			unsigned int tmp;
			if (my_distribution(my_generator) < (target - low_target)) {
				tmp = low_target + 1 + cum_frontier_int[i-1];
			} else {
				tmp = low_target + cum_frontier_int[i-1];
			}
			cum_frontier_int.push_back(tmp);
			std::cout << tmp << " ";
		}
		std::cout <<std::endl;
		
		if (cum_frontier_int[cum_frontier_int.size()-1] > size) {
			std::cout << "Reached MODEL_SIZE limit." << std::endl;
			model.cpu_model_str.size = index;
			break; // This is the limit we won't touch
		}

		std::vector<unsigned int> frontier_int;
		frontier_int.push_back(index);
		std::cout << "Building frontier_int from r="<<r-2<<" to r="<<r<<": ";
		std::cout << index << " ";
		for (unsigned int i=1;  i<cum_frontier_int.size(); i++) {
			frontier_int.push_back(cum_frontier_int[i] - cum_frontier_int[i-1]);
			std::cout << frontier_int[i] << " ";
		}
		std::cout << std::endl;

		// Get new_frontier ready
		for(unsigned int i=1; i<frontier_int.size(); i++) {
			std::set<unsigned int> addenda;
			FOR(j, frontier_int[i]) {
				addenda.insert(index+j);
				index++;
			}
			new_frontier[i] = addenda;
		}
		std::map<unsigned int, std::set<unsigned int>> old_new_frontier = new_frontier;

		// PART 2: linking the 1st link.
		std::cout << "Linking the first link." << std::endl;
		for(auto& id: old_new_frontier[1]) {
			std::set<unsigned int> last_frontier_set; unsigned int map_index;
			std::map<unsigned int, std::set<unsigned int>>::reverse_iterator it;
			for(it = frontier.rbegin(); it != frontier.rend() and (*it).second.empty(); it++); // puntiamo it a un set non vuoto
			if (it == frontier.rend() or (*it).first == 0) break; // Non possiamo linkare nient'altro
			else {
				map_index = (*it).first;
				last_frontier_set = (*it).second; // Nome più lungo ma più chiaro
			}
			unsigned int popped = *last_frontier_set.begin(); // Tanto è indifferente da dove cominciamo
			frontier[map_index].erase(popped); // pop
			frontier[map_index-1].insert(popped);
			model.safe_link(id, popped); // Abbiamo linkato un elemento della vecchia frontiera con un nuovo id.
			new_frontier[1].erase(id);
		}
		// Let's link the first link between themselves
		// Il primo link indica la posizione, in caso di ambiguità aspetto il secondo, e così via...
		std::cout << "Linking new_frontier[1] between themselves." << std::endl;
		for(auto &id: old_new_frontier[1]) {
			if (new_frontier[1].count(id)) continue; // Ci pensiamo dopo
			std::set<unsigned int> neighbours = model.get_neighbours(id);
			bool primary = false;
			for (auto &nid: neighbours) {
				if (old_frontier[1].count(nid)) {
					primary = true;
					break;
				}
			}
			if (not primary) continue;

			unsigned int expected_links;
			// Calculate it
			auto my_distribution = std::uniform_real_distribution<double>();
			uint low_bound = int(d*2) - 1; // Outer is taken for granted
			double fp = d*2 - low_bound - 1;
			if (my_distribution(my_generator) > fp) {
				expected_links = low_bound;
			} else {
				expected_links = low_bound + 1;
			}
			
			std::set<unsigned int> linked_roots;
			std::set<unsigned int> neigh2 = model.get_neighbours(id, 2);
			std::vector<unsigned int> neigh2vec(neigh2.begin(), neigh2.end());

			FOR(i, expected_links-neighbours.size()) { // Something might already be linked
				if (neigh2vec.empty()) break;
				auto rand_distr = std::uniform_int_distribution<unsigned int>(0, neigh2vec.size()-1);
				uint rand_index = rand_distr(my_generator);
				uint linkando_root = neigh2vec[rand_index];
				bool flag_root_primary = false;
				if (old_frontier[1].count(linkando_root)) flag_root_primary = true;
				

				neigh2vec.erase(neigh2vec.begin()+rand_index);
				auto neigh2neigh = model.get_neighbours(linkando_root);
				uint linkando = 0;
				for (auto &nnid: neigh2neigh) {
					if(old_new_frontier[1].count(nnid)) {
						auto nnnset = model.get_neighbours(nnid);
						if (not flag_root_primary and nnnset.size() > 1) continue;
						else {
							linkando = nnid;
							break;
						}
					}
				}
				if (linkando) {
					model.safe_link(linkando, id);
				} else {
					// Fregati: il linkando_root non ci può offrire un nodo vergine
					i--;
				}
			}
			neighbours = model.get_neighbours(id);
			if (neighbours.size()-1 != expected_links) { // We have a problem
				std::cout << "We have a problem for id="<<id<<": only "<<neighbours.size()-1<<"/"<<expected_links << " links built." <<std::endl;
			}
			std::cout << "Built id="<<id<<" links:";
			for(auto &n: neighbours) {
				std::cout << " " << n;
			}
			std::cout << std::endl;
		}

		// Ora tre alternative:
		//  * abbiamo linkato tutta la vecchia frontiera e tutta new_frontier (culo)
		//  * ci avanza roba dalla new_frontier[1] ancora da legare a qualcosa (sprout) // Facciamo in modo che non ci sia
		//  * abbiamo esaurito new_frontier[1] ma ancora c'è spazio in frontier per legare qualcosa (seal)
		
		// PART 3: Linking all the other old_new_frontier[i] spins
		// It's the same algorithm as above: 
		//  1. Find a suitable spin from new_frontier[i-1] that can link with me
		//  2. Link with that spin and with other i-1 of its neighbours that also belong to new_frontier[i-1]
		//  3. Link the new_frontier[i] already placed.
		//  4. Sprout new spins or seal the ones of new_frontier[i-1] so that we are back in situation (culo) for this layer

		

		//frontier = std::map<unsigned int, std::set<unsigned int>> ();
		frontier = old_new_frontier;
		std::cout << std::endl;
	}
	
	// TODO
}


int main(int argc, char* argv[]) {
	
	// argv = [ dim, model_size, sim_time, beta ]
	
	// Building the model
	unsigned int model_size = atoi(argv[2]);
	IsingModel model(model_size);
	
	double dim = atof(argv[1]);
	construct(model, dim);
	model_size = model.cpu_model_str.size;
	model.copy_graph_to_gpu();
	
	// Initializing a non-thermalized state
	FOR(i, model_size) {
		model.cpu_model_str.spins[i] = 1;
	}
	model.refresh_spins();

	model.beta = atof(argv[4]);
	
	// Simulating
	// TODO
	unsigned long long int sim_time = atoll(argv[3]);
	FOR(t, sim_time) {
		model.step_wolff_single_core();
		std::cout << "Magnetization at t="<<t<<": "<<model.get_magnetization()<<std::endl;
	}
	
	model.quit();
	return 0;
}

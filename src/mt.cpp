#ifndef FRAC_ISING_MT
#define FRAC_ISING_MT

#include <hip/hip_runtime.h>
#include "common.cpp"
#include <vector>
#include <new>

#ifdef GPU_COMPUTING

#include <rocRAND/hiprand>
#include <rocrand_mtgp32.h>

__constant__
struct {
	mtgp32_params_fast_t* cpu_constants_ptr;
	mtgp32_kernel_params_t* gpu_constants_ptr;
	hiprandStateMtgp32_t* gpu_states_array;
} generators;

__host__
void init_generators() {
	int n_blocks = N_BLOCKS;
	generators.cpu_constants_ptr = new mtgp32_params_fast_t;
	hipMalloc(& generators.gpu_constants_ptr, sizeof(mtgp32_kernel_params_t));

	hiprandMakeMTGP32Constants(generators.cpu_constants_ptr, generators.gpu_constants_ptr);
	
	hipMalloc(
			& generators.gpu_states_array,
			n_blocks*sizeof(hiprandStateMtgp32_t)
		);  // Note that threads within the same block use the same state, 
			// so we need one state per block (max 200 states)
	
	hiprandMakeMTGP32KernelState(
			generators.gpu_states_array,
			generators.cpu_constants_ptr,
			generators.gpu_constants_ptr,
			n_blocks,
			0 // seed 
		);
}

__forceinline__
__device__
float random_uniform_01() {
	return hiprand_uniform(& generators.gpu_states_array[blockIdx.x]);
}

__forceinline__
__device__
unsigned int random_unsigned() {
	return hiprand(& generators.gpu_states_array[blockIdx.x]);
}

#else

#include <random>
#include <mutex> // In realtà le cose vanno sullo stesso thread, dovrebbe essere tutto molto veloce
#include <array>

std::vector<std::uniform_int_distribution<unsigned int>> cpu_unsigned_uniforms;
std::vector<std::uniform_real_distribution<float>> cpu_float_uniforms;
std::vector<std::mt19937> cpu_generators;
std::mutex* cpu_mutexes;

__host__
void init_generators() {
	int n_blocks = N_BLOCKS;
	cpu_mutexes = new std::mutex[N_BLOCKS];
	FOR(i, n_blocks) {
		cpu_generators.push_back(std::mt19937(i));
		// cpu_mutexes[i] = std::mutex();
		cpu_unsigned_uniforms.push_back(std::uniform_int_distribution<unsigned int>()); // defaults to 0 - MAX
		cpu_float_uniforms.push_back(std::uniform_real_distribution<float>(0,1));
	}
}

__device__
float random_uniform_01() {
	cpu_mutexes[blockIdx.x].lock();
	float result = cpu_float_uniforms[blockIdx.x](cpu_generators[blockIdx.x]);
	cpu_mutexes[blockIdx.x].unlock();
	return result;
}

__device__
unsigned int random_unsigned() {
	cpu_mutexes[blockIdx.x].lock();
	unsigned int result = cpu_unsigned_uniforms[blockIdx.x](cpu_generators[blockIdx.x]);
	cpu_mutexes[blockIdx.x].unlock();
	return result;
}
		

#endif

#endif

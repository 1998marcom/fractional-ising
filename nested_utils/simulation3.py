import subprocess
import pathlib
from copy import deepcopy
import numpy as np

base_executable = ""

class FieldSimulation:
    
    args = {}
    path = ""

    ub = np.empty((0,3,), dtype=float) 
        # è moralmente una lista di 3 array [field, beta, u_blocked], 
        # ma il primo indice è la riga (punto), il secondo selezione l'array di cui parliamo
    ur = np.empty((0,3,), dtype=float)
    mb = np.empty((0,3,), dtype=float)
    mr = np.empty((0,3,), dtype=float)
    sb = np.empty((0,3,), dtype=float)
    sr = np.empty((0,3,), dtype=float)

    def __init__(self, path, args={}):

        self.path = pathlib.Path(path)

        if args:
            # Allora è da lanciare
            self.path.mkdir()
            self.args = args
            with open(self.path/"header", "w") as header:
                for name, arg in args.items():
                    header.write("{a} {b}\n".format(a=name, b=arg))
            sim_args = deepcopy(self.args)
            _maxfield = float(self.args["field"])
            _fieldruns = int(self.args["fieldruns"])
            del sim_args["field"]
            del sim_args["fieldruns"]
            fields = np.linspace(0, _maxfield, _fieldruns)
            for i in range(_fieldruns):
                print("Fieldrun {} of {}.".format(i, _fieldruns))
                s = Simulation(self.path / "fieldrun_{:03d}".format(i), sim_args)
                for j in range(len(s.bb)):
                    self.ub = np.append(self.ub, np.array([fields[i], s.bb[j], s.u_blocked[j]]).reshape((1,3,)), axis=0)
                    self.ur = np.append(self.ur, np.array([fields[i], s.bb[j], s.u_reduced[j]]).reshape((1,3,)), axis=0)
                    self.mb = np.append(self.mb, np.array([fields[i], s.bb[j], s.m_blocked[j]]).reshape((1,3,)), axis=0)
                    self.mr = np.append(self.mr, np.array([fields[i], s.bb[j], s.m_reduced[j]]).reshape((1,3,)), axis=0)
                    self.sb = np.append(self.sb, np.array([fields[i], s.bb[j], s.s_blocked[j]]).reshape((1,3,)), axis=0)
                    self.sr = np.append(self.sr, np.array([fields[i], s.bb[j], s.s_reduced[j]]).reshape((1,3,)), axis=0)
            np.savetxt(self.path/"ub", self.ub)
            np.savetxt(self.path/"ur", self.ur)
            np.savetxt(self.path/"mb", self.mb)
            np.savetxt(self.path/"mr", self.mr)
            np.savetxt(self.path/"sb", self.sb)
            np.savetxt(self.path/"sr", self.sr)
        else:
            # Se è solo da parsare
            with open(self.path/"header", "r") as header:
                for line in header.readlines():
                    words = line.split()
                    self.args[words[0]] = words[1]
            self.ub = np.loadtxt(self.path/"ub")
            self.ur = np.loadtxt(self.path/"ur")
            self.mb = np.loadtxt(self.path/"mb")
            self.mr = np.loadtxt(self.path/"mr")
            self.sb = np.loadtxt(self.path/"sb")
            self.sr = np.loadtxt(self.path/"sr")




class Simulation:

    args = {}
    path = ""

    bb = np.array([])
    u_blocked = np.array([])
    u_reduced = np.array([])
    m_blocked = np.array([])
    m_reduced = np.array([])
    s_blocked = np.array([])
    s_reduced = np.array([])
    
    def __init__(self, path, args={}):

        self.path = pathlib.Path(path)
        
        if args:
            # Allora è da lanciare
            self.path.mkdir()
            self.args = args
            with open(self.path/"header", "w") as header:
                for name, arg in args.items():
                    header.write("{a} {b}\n".format(a=name, b=arg))
            iter_args = deepcopy(self.args)
            _iter = int(self.args["iter"])
            _runs = int(self.args["runs"])
            _selected_runs = int(self.args["selected-runs"]) # We are going to select 2*selected_runs
            del iter_args["iter"]
            del iter_args["runs"]
            iter_args["length"] = self.args["runs"]
            del iter_args["selected-runs"]
            _min = float(self.args["min"])
            _max = float(self.args["max"])
            for it in range(_iter):
                print("Beginning iter {} of {}.".format(it, _iter), flush=True)
                if it == 0:
                    iter_args["min"] = _min
                    iter_args["max"] = _max
                else:
                    def find_crit_index(a, b):
                        return (a < b).sum()
                        '''
                        a1 = a[::-1]
                        b1 = b[::-1]
                        if len(a1) != len(b1):
                            raise KeyError
                        for i in range(len(a1)):
                            if a1[i] > b1[i]:
                                return len(a1)-i-1
                        raise KeyError
                        '''
                    ix = find_crit_index(self.m_blocked, self.m_reduced)
                    iter_args["min"] = self.bb[max(0, ix-_selected_runs)]
                    iter_args["max"] = self.bb[min(len(self.bb)-1, ix+1+_selected_runs)]
                    iter_args["min"] = iter_args["min"]*1.03-0.03*iter_args["max"]
                    iter_args["max"] = iter_args["max"]*1.03-0.03*iter_args["min"]
                if args:
                    iteration = Iteration(self.path/"iter{:03d}".format(it), iter_args)
                else:
                    iteration = Iteration(self.path/"iter{:03d}".format(it))
                self.bb = np.concatenate([self.bb, iteration.bb])
                self.u_blocked = np.concatenate([self.u_blocked, iteration.u_blocked])
                self.u_reduced = np.concatenate([self.u_reduced, iteration.u_reduced])
                self.m_blocked = np.concatenate([self.m_blocked, iteration.m_blocked])
                self.m_reduced = np.concatenate([self.m_reduced, iteration.m_reduced])
                self.s_blocked = np.concatenate([self.s_blocked, iteration.s_blocked])
                self.s_reduced = np.concatenate([self.s_reduced, iteration.s_reduced])
                sorting_indexes = np.argsort(self.bb)
                self.bb = self.bb[sorting_indexes]
                self.u_blocked = self.u_blocked[sorting_indexes]
                self.u_reduced = self.u_reduced[sorting_indexes]
                self.m_blocked = self.m_blocked[sorting_indexes]
                self.m_reduced = self.m_reduced[sorting_indexes]
                self.s_blocked = self.s_blocked[sorting_indexes]
                self.s_reduced = self.s_reduced[sorting_indexes]
            np.savetxt(self.path/"u", np.column_stack((self.bb, self.u_blocked, self.u_reduced)))
            np.savetxt(self.path/"m", np.column_stack((self.bb, self.m_blocked, self.m_reduced)))
            np.savetxt(self.path/"s", np.column_stack((self.bb, self.s_blocked, self.s_reduced)))
        else:
            # Se è solo da parsare
            with open(self.path/"header", "r") as header:
                for line in header.readlines():
                    words = line.split()
                    self.args[words[0]] = words[1]
            self.bb, self.u_blocked, self.u_reduced = np.loadtxt(self.path/"u", unpack=True)
            self.bb, self.m_blocked, self.m_reduced = np.loadtxt(self.path/"m", unpack=True)
            self.bb, self.s_blocked, self.s_reduced = np.loadtxt(self.path/"s", unpack=True)

class Iteration:
    
    args = {}
    path = ""
    
    def __init__(self, path, args={}):
        
        self.path = pathlib.Path(path)
        
        if args:
            # Allora è anche da lanciare
            self.path.mkdir()
            self.args = args
            with open(path/"header", "w") as header:
                for name, arg in args.items():
                    header.write("{} {}\n".format(name, arg))
            run_args = deepcopy(self.args)
            _min = float(self.args["min"])
            _max = float(self.args["max"])
            _l = int(self.args["length"])
            del run_args["min"]
            del run_args["max"]
            del run_args["length"]
            bb = np.linspace(_min, _max, _l)
            self.bb = bb
            print("At this iteration bb is", bb)

            uF = open(self.path / "u", "w")
            uF.write("#blocked reduced\n")
            self.u_blocked = []; self.u_reduced = [] # array of averages of single runs
            mF = open(self.path / "m", "w")
            mF.write("#blocked reduced\n")
            self.m_blocked = []; self.m_reduced = []
            sF = open(self.path / "s", "w")
            sF.write("#blocked reduced\n")
            self.s_blocked = []; self.s_reduced = []

            for index, b in enumerate(bb):
                print("\tPerforming run {} of {}.".format(index, len(bb)), flush=True)
                run_args["beta"] = b
                run_path_a = self.path / "{:03d}a".format(index)
                run_a = Run(run_path_a, run_args) 
                run_args_b = deepcopy(run_args)
                run_args_b["scale"] = str(int(run_args["scale"])-1)
                run_path_b = self.path / "{:03d}b".format(index)
                run_b = Run(run_path_b, run_args_b) 
                # Noi saremmo interessati a run_a_blocked confrontato con run_b
                u_blocked, m_blocked = run_a.data_blocked
                u_reduced, m_reduced = run_b.data
                self.u_blocked.append(u_blocked.mean())
                self.u_reduced.append(u_reduced.mean())
                self.m_blocked.append(abs(m_blocked).mean())
                self.m_reduced.append(abs(m_reduced).mean())
                self.s_blocked.append(m_blocked.mean())
                self.s_reduced.append(m_reduced.mean())
                uF.write("{beta} {blocked} {reduced}\n".format(beta=b, blocked=self.u_blocked[-1], reduced=self.u_reduced[-1]))
                mF.write("{beta} {blocked} {reduced}\n".format(beta=b, blocked=self.m_blocked[-1], reduced=self.m_reduced[-1]))
                sF.write("{beta} {blocked} {reduced}\n".format(beta=b, blocked=self.s_blocked[-1], reduced=self.s_reduced[-1]))
            uF.close()
            mF.close()
            sF.close()

        else:
            # Se è solo da parsare
            with open(path/"header", "r") as header:
                for line in header.readlines():
                    words = line.split()
                    self.args[words[0]] = words[1]
            self.bb, self.u_blocked, self.u_reduced = np.loadtxt(path/"u", unpack=True)
            self.bb, self.m_blocked, self.m_reduced = np.loadtxt(path/"m", unpack=True)
            self.bb, self.s_blocked, self.s_reduced = np.loadtxt(path/"s", unpack=True)

class Run:
    args = {}
    path = ""
    def __init__(self, path, args={}):
        if args:
            # Allora è anche da lanciare
            arg_list = [base_executable]
            for name, arg in args.items():
                arg_list.extend(["--"+name, arg])
            if "-o" not in arg_list:
                arg_list.extend(["-o", path])
            arg_list = list(map(str, arg_list))
            self.path = path
            subprocess.run(arg_list, stdout=subprocess.DEVNULL)
        # Ora è da parsare
        with open(str(path)+"_header","r") as headerF:
            for line in headerF.readlines():
                words = line.split()
                self.args[words[0]] = words[1]
        self.path = path
        self.data = np.loadtxt(str(path), unpack=True) # self.data = beta, energy, magnetization
        self.data_blocked = np.loadtxt(str(path)+"_blocked", unpack=True)
        # Seghiamo la termalizzazione sotto buone ipotesi:
        energy, *_ = self.data
        condition = (energy > np.median(energy))
        intcond = list(map(int, list(condition)))
        first_ix = intcond.index(1)
        self.data = tuple(column[first_ix:] for column in self.data)
        self.data_blocked = tuple(column[first_ix:] for column in self.data_blocked)

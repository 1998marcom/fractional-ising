#!/usr/bin/env python3

import sys, os
import subprocess as s
import argparse
from tempfile import mkstemp
from sim_parser import simulation_parser
import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Plots abs_magnetization vs beta.')
parser.add_argument('-m', '--min', required=True, type=float)
parser.add_argument('-M', '--max', required=True, type=float)
parser.add_argument('-l', '--length', default=20, type=int, help='Number of beta values to try')
parser.add_argument('-t', '--time', required=True, type=int)
parser.add_argument('-s', '--scale', required=True, type=int)
parser.add_argument('-e', '--extra', required=True, type=int)
parser.add_argument('-n', '--nsplit', required=True, type=int)
parser.add_argument('-d', '--env_dim', required=True, type=int)
parser.add_argument('-o', '--outfile', type=str) # TODO
parser.add_argument('simulation_executable', type=str)
args = parser.parse_args()

beta_array = np.linspace(args.min, args.max, args.length)
avg_mag_blocked = []
avg_ene_blocked = []
avg_mag_reduced = []
avg_ene_reduced = []

index = 1
for beta in beta_array:
    print('Performing simulation', '{}/{}'.format(index, len(beta_array)))
    garbage, tempfile_path = mkstemp()
    
    s.run(
            map(str, [
                './'+args.simulation_executable, 
                '-t', args.time, 
                '-s', args.scale, 
                '-e', args.extra, 
                '-n', args.nsplit, 
                '-d', args.env_dim, 
                '-o', tempfile_path, 
                '-b', beta
            ])
    )
    for i in range(0, 5):
        try:
            ene, mag = simulation_parser(tempfile_path+"_blocked")
        except KeyError:
            s.run(map(str, ['./'+args.simulation_executable, '-t', args.time+(2**i)*10000, '-s', args.scale, '-e', args.scale, '-n', args.nsplit, '-d', args.env_dim, '-o', tempfile_path, '-b', beta]))
        else:
            break
    avg_mag_blocked.append(abs(mag).mean())
    avg_ene_blocked.append(ene.mean())

    s.run(
            map(str, [
                './'+args.simulation_executable, 
                '-t', args.time, 
                '-s', args.scale-1, 
                '-e', args.extra, 
                '-n', args.nsplit, 
                '-d', args.env_dim, 
                '-o', tempfile_path, 
                '-b', beta
            ])
    )
    for i in range(0, 5):
        try:
            ene, mag = simulation_parser(tempfile_path)
        except KeyError:
            s.run(map(str, ['./'+args.simulation_executable, '-t', args.time+(2**i)*10000, '-s', args.scale, '-e', args.scale-1, '-n', args.nsplit, '-d', args.env_dim, '-o', tempfile_path, '-b', beta]))
        else:
            break
    avg_mag_reduced.append(abs(mag).mean())
    avg_ene_reduced.append(ene.mean())
    
    os.remove(tempfile_path)
    index += 1

plt.plot(beta_array, avg_ene_reduced, label='Reduced')
plt.plot(beta_array, avg_ene_blocked, label='Blocked')
plt.legend()
plt.title('Energy graphs')
plt.show()

plt.plot(beta_array, avg_mag_reduced, label='Reduced')
plt.plot(beta_array, avg_mag_blocked, label='Blocked')
plt.legend()
plt.title('Magnetization graphs')
plt.show()


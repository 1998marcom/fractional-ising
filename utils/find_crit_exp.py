#!/usr/bin/env python3

import sys, os
import subprocess as s
import argparse
from tempfile import mkstemp
from sim_parser import simulation_parser
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

parser = argparse.ArgumentParser(description='Plots magnetization vs tau, and fits.')
parser.add_argument('-m', '--min', required=True, type=float)
parser.add_argument('-M', '--max', required=True, type=float)
parser.add_argument('-b', '--betacrit', required=True, type=float)
parser.add_argument('-l', '--length', default=20, type=int, help='Number of beta values to try')
parser.add_argument('-t', '--time', required=True, type=int)
parser.add_argument('-s', '--scale', required=True, type=int)
parser.add_argument('-e', '--extra', required=True, type=int)
parser.add_argument('-n', '--nsplit', required=True, type=int)
parser.add_argument('-d', '--env_dim', required=True, type=int)
parser.add_argument('-o', '--outfile', type=str) # TODO
parser.add_argument('simulation_executable', type=str)
args = parser.parse_args()

Tcrit = 1/args.betacrit
beta_array = np.linspace(args.min, args.max, args.length)
tau_array = (1/beta_array-Tcrit)/(1/args.betacrit)


mag_array = []
index = 0
for beta in beta_array:
    print("Performing simulation {}/{}".format(index+1, len(beta_array)))
    garbage, path = mkstemp()
    s.run(
        map(str, [
            './'+args.simulation_executable,
            '-t', args.time,
            '-s', args.scale,
            '-e', args.extra,
            '-n', args.nsplit,
            '-d', args.env_dim,
            '-o', path,
            '-b', beta
        ])
    )
    mags = simulation_parser(path)
    mag_array.append(abs(mags).mean())
    os.remove(path)
    index += 1

if args.outfile:
    np.savetxt(args.outfile, np.array(mag_array))

print("Given Tcrit", Tcrit)
def halffitfunc(tt, k, gamma):
    tau = (tt-Tcrit)/Tcrit
    return k*pow(np.maximum(np.zeros(len(tau)), -tau), gamma)
def fitfunc(tt, k, Tc, gamma):
    tau = (tt-Tc)/Tc
    return k*pow(np.maximum(np.zeros(len(tau)), -tau), gamma)
def overfitfunc(tt, k, Tc, gamma, offset):
    tau = (tt-Tc)/Tc
    return k*pow(np.maximum(np.zeros(len(tau)), -tau), gamma)


p0 = (2, 0.12,)
pp0 = (2, Tcrit, 0.2)
ppp0 = (2, Tcrit, 0.2, 0.0)

popt, pcov = curve_fit(halffitfunc, 1/beta_array, mag_array, p0)
print("Halffit result:\n\tk:", popt[0], "\n\tgamma:", popt[1])
ppopt, ppcov = curve_fit(fitfunc, 1/beta_array, mag_array, pp0)
print("Halffit result:\n\tk:", ppopt[0], "\n\tTc", ppopt[1], "\n\tgamma:", ppopt[2])
pppopt, pppcov = curve_fit(overfitfunc, 1/beta_array, mag_array, ppp0)
print("Halffit result:\n\tk:", pppopt[0], "\n\tTc", pppopt[1], "\n\tgamma:", pppopt[2], "\n\toffset:", pppopt[3])

tt = np.linspace(min(1/beta_array), max(1/beta_array), 100)
plt.plot(tau_array, mag_array, label='data: $\\tau$@Tcrit')
plt.plot((tt-Tcrit)/Tcrit, halffitfunc(tt, *popt), label='halffit')

plt.plot((tt-pp0[1])/pp0[1], fitfunc(tt, *pp0), label='prefit')
plt.plot((1/beta_array-ppopt[1])/ppopt[1], mag_array, label='data: $\\tau$@Tc')
plt.plot((tt-ppopt[1])/ppopt[1], fitfunc(tt, *ppopt), label='fit')
plt.plot((1/beta_array-pppopt[1])/pppopt[1], mag_array, label='data: $\\tau$@Tcover')
plt.plot((tt-pppopt[1])/pppopt[1], overfitfunc(tt, *pppopt), label='overfit')
plt.legend()
plt.show()

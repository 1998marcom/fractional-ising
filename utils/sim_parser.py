
import numpy as np

def simulation_parser(path):
    '''A function that, given a path, returns normalized parameters
    '''
    
    fp = open(path+"_header", "r")
    fp.readline()
    env_dim, n, extra_occ, scale, sim_time, beta, J, B, pflip, model_size = fp.readline().split()
    env_dim = int(env_dim)
    n = int(n)
    extra_occ = int(extra_occ)
    scale = int(scale)
    sim_time = int(sim_time)
    beta = float(beta)
    J = float(J)
    B = float(B)
    pflip = float(pflip)
    model_size = int(model_size)
    fp.close()

    t_magnetization = np.loadtxt(path, unpack=True)
    magnetization = t_magnetization / model_size
    for i, mag in enumerate(magnetization):
        if mag < 0:
            magnetization = magnetization[i:]
            break
        if i == len(magnetization)//2:
            raise KeyError()
    return magnetization


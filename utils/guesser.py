#!/usr/bin/env python3

# Da chiamare come:
# ./guesser.py DIMENSIONS+1 RADIUS+1
# Stampa il rapporto tra il volume e il numero di punti di un reticolo a DIMENSION dimensioni,
# all'interno di una palla di raggio RADIUS, con RADIUS misurato con la distanza-1 sui link

from scipy.special import gamma
from scipy.optimize import curve_fit
import numpy as np
import sys
from pprint import pprint
import matplotlib.pyplot as plt

def volume(radius, dimension):
    return pow(2, dimension)*pow(radius, dimension)/gamma(dimension+1)

class Dimension:
    vol = [0]
    dots = [1]

dimensions = [Dimension()]

J = int(sys.argv[2])
max_dim = int(sys.argv[1])
for i in range (1,max_dim):
    dimensions.append(Dimension())
    dimensions[-1].vol = [volume(r, i) for r in range(J)]
    if i==1:
        dimensions[-1].dots = [1+2*r for r in range(J)]
    else:
        dimensions[-1].dots = [1]
        dimensions[-1].dots.append(2*1+dimensions[-2].dots[1])
        for r in range(2, J):
            dimensions[-1].dots.append(((dimensions[-1].dots[r-1]-dimensions[-2].dots[r-1])/2+dimensions[-2].dots[r-1])*2+dimensions[-2].dots[r])
    print("Dimension ", i)
    print(dimensions[-1].vol[-1]/dimensions[-1].dots[-1])
    #pprint(dimensions[-1].dots)
xx = np.linspace(0, J, 1000)
plt.plot(xx, volume(xx,max_dim-1), color="red", label="volume")
plt.scatter(range(J), dimensions[max_dim-1].dots, color="blue", label="dots")
plt.plot(range(J), [0]*J, color="black")

def dotsest(radius, k, offset):
    dimension = max_dim-1
    return k*pow(2, dimension)*pow(radius+offset, dimension)/gamma(dimension+1)

pp0 = (1, 0)
popt, pcov = curve_fit(dotsest, np.array(range(J)), np.array(dimensions[max_dim-1].dots), p0=pp0)

chi2 = sum((dotsest(np.array(range(J)), *popt) - np.array(dimensions[max_dim-1].dots))**2)
plt.plot(range(J), dotsest(np.array(range(J)), *popt), color="green", label="fit with chi2={}".format(chi2))

def binomial(n, k):
    return gamma(n+1)/gamma(k+1)/gamma(n-k+1)
def paolo(radius):
    r = radius; d = max_dim - 1
    #print(gamma(r+d-1))
    #print(gamma(d), gamma(r))
    #front = 2*d*gamma(r+d)/gamma(d+1)/gamma(r)
    #return np.array([front[:i].sum() for i in range(1, 1+len(front))])
    #front = pow(2, d)*gamma(r+d-1)/gamma(d)/gamma(r)
    #return np.array([front[:i].sum() for i in range(1, 1+len(front))])
    #front += ( pow(2, d) - 2*d ) * (r>1.5)* (gamma(d+r-2)/gamma(d+1)/gamma(r-2))
    #return front
    front = 0.*radius
    for m in range(d):
        front += pow(2, d-m)*binomial(d, m)*binomial(r-1, d-1-m)
    return np.array([front[:i].sum() for i in range(1, 1+len(front))])
    #return front
    
chi3 = sum((paolo(np.array(range(1,J)))-np.array(dimensions[max_dim-1].dots[1:]))**2)
plt.plot(range(1, J), paolo(np.array(range(1, J))), color="purple", label="paolo with chi2={}".format(chi3))


plt.xlabel("radius")
plt.ylabel("dots count / radius^d")
plt.legend()
plt.title("Dimension {} benchmark".format(max_dim-1))
plt.show()

#!/usr/bin/env python3

import sys, os
import subprocess as s
import argparse
from tempfile import mkstemp
from sim_parser import simulation_parser
import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Plots abs_magnetization vs beta.')
parser.add_argument('-m', '--min', required=True, type=float)
parser.add_argument('-M', '--max', required=True, type=float)
parser.add_argument('-l', '--length', default=20, type=int, help='Number of beta values to try')
parser.add_argument('-t', '--time', required=True, type=int)
parser.add_argument('-S', '--maxscale', required=True, type=int)
parser.add_argument('-s', '--minscale', required=False, default=1, type=int)
parser.add_argument('-e', '--extra', required=True, type=int)
parser.add_argument('-n', '--nsplit', required=True, type=int)
parser.add_argument('-d', '--env_dim', required=True, type=int)
parser.add_argument('-o', '--outfile', type=str) # TODO
parser.add_argument('simulation_executable', type=str)
args = parser.parse_args()

beta_array = np.linspace(args.min, args.max, args.length)
avg_magnetizations = []
for scale_reduction in range(0,args.maxscale-args.minscale+1):
    avg_magnetizations.append([])
index = 1
for beta in beta_array:
    print('Performing simulation', '{}/{}'.format(index, len(beta_array)))
    garbage, tempfile_path = mkstemp()
    for scale_reduction in range(0,args.maxscale-args.minscale+1):
        s.run(
                map(str, [
                    './'+args.simulation_executable, 
                    '-t', args.time, 
                    '-s', args.maxscale-scale_reduction, 
                    '-e', args.extra, 
                    '-n', args.nsplit, 
                    '-d', args.env_dim, 
                    '-o', tempfile_path, 
                    '-b', beta
                ])
        )
        for i in range(0, 5):
            try:
                magnetization = simulation_parser(tempfile_path)
            except KeyError:
                s.run(map(str, ['./'+args.simulation_executable, '-t', args.time+(2**i)*10000, '-s', args.scale, '-e', args.maxscale-scale_reduction, '-n', args.nsplit, '-d', args.env_dim, '-o', tempfile_path, '-b', beta]))
            else:
                break
        avg_magnetizations[scale_reduction].append(abs(magnetization).mean())
    os.remove(tempfile_path)
    index += 1

index = 0
for avg in avg_magnetizations:
    plt.plot(beta_array, avg, label='scale = '+str(args.maxscale-index))
    index += 1
plt.legend()
plt.show()


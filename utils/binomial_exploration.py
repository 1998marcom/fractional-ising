#!/usr/bin/env python3

# Da chiamare come:
# ./guesser.py DIMENSIONS+1 RADIUS+1
# Stampa il rapporto tra il volume e il numero di punti di un reticolo a DIMENSION dimensioni,
# all'interno di una palla di raggio RADIUS, con RADIUS misurato con la distanza-1 sui link

from scipy.special import gamma
from scipy.optimize import curve_fit
import numpy as np
import sys
from pprint import pprint
import matplotlib.pyplot as plt
from copy import deepcopy

#np.seterr(all='raise')
max_dim = int(sys.argv[1])
r = int(sys.argv[2])

def binomial(n, k):
    return gamma(n+1)/gamma(k+1)/gamma(n-k+1)

def truexp(a):
    return np.power(r+2, a)

def sumtoint(a):
    #print("Evaluating sumtoint at radius", r, ", dim", d)
    m = 0
    result = np.zeros(len(a))
    while (a[-1] >= m):
        result += (a >= m) * pow(r, a-m) * pow(2, m) * binomial(a, m)
        m += 1
    return result

xx = np.array(range(1, max_dim))
xxx = np.linspace(1, max_dim-1, 1000)
plt.plot(xxx, sumtoint(xxx), color="red", label="binomial expansion")
plt.plot(xxx, truexp(xxx), color="blue", label="true power")

pp0 = (1., 0.2, max_dim-2, 0.)
#popt, pcov = curve_fit(sumtoint, np.array(range(1, J)), np.array(paolo(np.array(range(1,J)))), p0=pp0)
#print("popt =", popt)




plt.xlabel("exponent")
plt.ylabel("value [pure number]")
plt.legend()
plt.title("Radius {} benchmark".format(r))
plt.show()

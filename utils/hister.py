#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt

magnetization_array = np.loadtxt(sys.argv[1], unpack=True)

plt.hist(magnetization_array, bins=20)
print(abs(magnetization_array).mean())

plt.show()

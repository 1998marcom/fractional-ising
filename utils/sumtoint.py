#!/usr/bin/env python3

# Da chiamare come:
# ./guesser.py DIMENSIONS+1 RADIUS+1
# Stampa il rapporto tra il volume e il numero di punti di un reticolo a DIMENSION dimensioni,
# all'interno di una palla di raggio RADIUS, con RADIUS misurato con la distanza-1 sui link

from scipy.special import gamma
from scipy.optimize import curve_fit
import numpy as np
import sys
from pprint import pprint
import matplotlib.pyplot as plt

np.seterr(all='raise')
max_dim = int(sys.argv[1])
J = int(sys.argv[2])

def binomial(n, k):
    return gamma(n+1)/gamma(k+1)/gamma(n-k+1)
def paolo(radius):
    r = radius; d = max_dim - 1
    #print("Evaluating paolo at radius", r, ", dim", d)
    front = 0.*radius
    for m in range(d):
        front += pow(2, d-m)*binomial(d, m)*binomial(r-1, d-1-m)
    return np.array([front[:i].sum() for i in range(1, 1+len(front))])

def sumtoint(radius, k, start, end, q):
    r = radius; d = max_dim - 1
    #print("Evaluating sumtoint at radius", r, ", dim", d)
    front = 0.*radius
    MM = 1000
    mm = np.linspace(start, end, MM+1)
    for m in mm[:-1]:
        try:
            front += (d-1)/MM*pow(2, d-m)*binomial(d, m)*binomial(r-1, d-1-m)
        except FloatingPointError:
            print("caught exception when r={0}, d={1}, m={2}".format(r, d, m))
    return k*np.array([front[:i].sum() for i in range(1, 1+len(front))])+q

xx = np.array(range(1, J)) #1, J-1, 1000)
plt.scatter(range(1, J), paolo(np.array(range(1,J))), color="blue", label="paolo points")
plt.plot(range(J), [0]*J, color="black")

pp0 = (1., 0.2, max_dim-2, 0.)
popt, pcov = curve_fit(sumtoint, np.array(range(1, J)), np.array(paolo(np.array(range(1,J)))), p0=pp0)
print("popt =", popt)
plt.plot(xx, sumtoint(xx, *pp0), color="green", label="sumtoint")
chi2 = sum((paolo(xx)-sumtoint(xx, *popt))**2)
plt.plot(xx, sumtoint(xx, *popt), color="orange", label="sumtoint fit, chi2={}".format(chi2))




plt.xlabel("radius")
plt.ylabel("dots count")
plt.legend()
plt.title("Dimension {} benchmark".format(max_dim-1))
plt.show()


import numpy as np

def simulation_parser(path):
    '''A function that, given a path, returns normalized parameters
    '''
    
    ene, mag = np.loadtxt(path, unpack=True)
    for i, m in enumerate(mag):
        absmean = abs(mag).mean()
        if abs(m) <= absmean:
            mag = mag[i:]
            ene = ene[i:]
            break
        if i == len(mag)//2:
            raise KeyError()
    return ene, mag

